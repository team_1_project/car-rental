﻿using CarRental.Core.Common;
using CarRental.Core.Models;
using System;
using System.Security.Policy;

namespace CarRental.MVC.Models
{
    public class EmailTemplate
    {
        public static string ForgotPasswordEmail(string? url, string? name = "")
        {
            name = name ?? string.Empty;

            var result = $@"
Hi {name},

There was a request to change your password!

If you did not make this request then please ignore this email.

Otherwise, please click this link to change your password: {url}";
            return result;
        }
        public static string ConfirmedBooking(Booking? booking, string? bookingNo = "", string? name = "")
        {
            name = name ?? string.Empty;
            bookingNo = bookingNo ?? string.Empty;

            // Conditionally create an HTML table if a valid Booking object is provided
            var bookingTable = booking != null ? $@"
        <table class='table table-bordered'>
            <tr>
                <th>Booking Number</th>
                <td>{booking.BookingNo}</td>
            </tr>
            <tr>
                <th>Start Date and Time</th>
                <td>{booking.StartDateTime}</td>
            </tr>
            <tr>
                <th>End Date and Time</th>
                <td>{booking.EndDateTime}</td>
            </tr>
            <tr>
                <th>Payment Method</th>
                <td>{booking.PaymentMethod}</td>
            </tr>
            <tr>
                <th>Payment Status</th>
                <td>{booking.Status}</td>
            </tr>
            <tr>
                <th>Deposit</th>
                <td>{FormatHelper.FormatCurrency(booking.Car.Deposit)}</td>
            </tr>
            <tr>
                <th>Total</th>
                <td>{FormatHelper.FormatCurrency(booking.Car.Deposit + (booking.Car?.BasePrice ?? 0) * (decimal)((booking.EndDateTime - booking.StartDateTime).TotalDays + 1))}</td>
            </tr>
        </table>
    " : string.Empty;

            // Inline CSS for styling
            var cssStyles = @"
        <style>
            .table {
                width: 100%;
                max-width: 600px;
                margin: 0 auto;
                border-collapse: collapse;
            }
            .table th, .table td {
                padding: 8px;
                border: 1px solid #ddd;
                text-align: left;
            }
            /* Add more CSS styles as needed */
        </style>
    ";

            // Construct the email message with CSS styles
            var result = $@"
        <html>
        <head>
            <title>Email</title>
            {cssStyles}
        </head>
        <body>
            <div>
                <p>Hi {name},</p>
                <p>You have successfully rented a car: {bookingNo}</p>
                {bookingTable}
                <p><a href='https://localhost:7117/Customer'>Go to homepage to confirm pickup if you received our car!</a></p>
            </div>
        </body>
        </html>
    ";

            return result;
        }
        public static string ReceivedDepositFromCustomer(string? totalDeposit, string? car = "", string? bookingNo = "", string? name = "")
        {
            name = name ?? string.Empty;

            var result = $@"
Hi {name}, <br>
You already have received a booking deposit <br>
In: {car} <br>
Total: {totalDeposit}  <br>
BookingNo: {bookingNo}";

            return result;
        }

        public static string CancelBookingFromCustomer(string? car = "", string? bookingNo = "", string? name = "")
        {
            name = name ?? string.Empty;

            var result = $@"
            Hi {name}, <br>
            Your car has been canceled booking! <br>
            In: {car} <br>
            BookingNo: {bookingNo}";

            return result;
        }
    }
}
