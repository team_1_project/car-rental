﻿using AutoMapper;
using CarRental.Core.Models;
using CarRental.Core.ViewModels;
using CarRental.MVC.Areas.Accounts.Models;
using CarRental.MVC.Areas.CarOwner.Models;
using CarRental.MVC.Areas.Customer.Models;

namespace CarRental.MVC.Models
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Car, CarViewModel>().ReverseMap();
            CreateMap<ApplicationUser, EditUserModel>().ReverseMap();
            CreateMap<BookingViewModel, Booking>().ReverseMap();
            CreateMap<CustomerCarViewModel, Car>().ReverseMap();
            CreateMap<Car, EditCarViewModel>().ReverseMap();
            CreateMap<Feedback, ReportViewModel>().ReverseMap();
        }
    }
}
