﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.MVC.Areas.Accounts.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
