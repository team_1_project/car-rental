﻿namespace CarRental.MVC.Areas.Accounts.Models
{
    public interface IEmailSender
    {
        void SendEmail(Message message, bool isHtml = false);

        Task SendEmailAsync(Message message, bool isHtml = false);
    }
}
