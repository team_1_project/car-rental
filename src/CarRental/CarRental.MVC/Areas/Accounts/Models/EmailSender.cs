﻿using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace CarRental.MVC.Areas.Accounts.Models
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailConfiguration _emailConfig;

        public EmailSender(EmailConfiguration emailConfig)
        {
            _emailConfig = emailConfig;
        }

        public async Task SendEmailAsync(Message message, bool isHtml = false)
        {
            var mailMessage = CreateEmailMessage(message, isHtml);

            await SendAsync(mailMessage);
        }

        public void SendEmail(Message message, bool isHtml = false)
        {
            var emailMessage = CreateEmailMessage(message, isHtml);

            Send(emailMessage);
        }

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);
                    await client.SendAsync(mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception, or both.
                    throw;
                }
                finally
                {
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }



        private void Send(MimeMessage emailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(_emailConfig.SmtpServer, _emailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_emailConfig.UserName, _emailConfig.Password);
                    client.Send(emailMessage);
                }
                catch
                {
                    //log an error message or throw an exception or both.
                    throw;
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }

        private MimeMessage CreateEmailMessage(Message message, bool isHtml)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Rent Car Team", _emailConfig.From));
            emailMessage.To.AddRange(message.To);
            emailMessage.Subject = message.Subject;

            TextPart textPart;
            if (isHtml)
            {
                textPart = new TextPart(TextFormat.Html)
                {
                    Text = message.Content
                };
            }
            else
            {
                textPart = new TextPart(TextFormat.Plain)
                {
                    Text = message.Content
                };
            }

            emailMessage.Body = textPart;

            return emailMessage;
        }
    }
}
