﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CarRental.Core.Models;

namespace CarRental.MVC.Areas.Accounts.Models
{
    public class EditUserModel
    {
        [Required]
        public string? Name { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string? PhoneNumber { get; set; }
        
        [Required]
        public string? NationalIdNo { get; set; }

        [Required]
        public Address? Address { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Max length is less than {0} characters")]
        [DisplayName("Detail address")]
        public string? DetailAddress { get; set; }
        
        [Required]
        public DateTime DateOfBirth { get; set; }

        [DataType(DataType.EmailAddress)]
        public string? EmailAddress { get; set; }

        public IFormFile? ImageFileDriverLicense { get; set; }

        public string? ImageUrlDriverLicense { get; set; }

        public ChangePasswordViewModel ChangePasswordViewModel { get; set; } = new ChangePasswordViewModel();
    }
}