﻿using AutoMapper;
using CarRental.Core.Common;
using CarRental.Core.Models;
using CarRental.MVC.Areas.Accounts.Models;
using CarRental.MVC.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.MVC.Areas.Accounts.Controllers
{
	[Area("Accounts")]

	public class AuthController : Controller
	{
		private readonly SignInManager<ApplicationUser> _signInManager;

		private readonly UserManager<ApplicationUser> _userManager;

		private readonly Models.IEmailSender _emailSender;

		private readonly IMapper _mapper;

		public AuthController(
			SignInManager<ApplicationUser> signInManager,
			UserManager<ApplicationUser> userManager,
			IEmailSender emailSender,
			IMapper mapper)
		{
			_signInManager = signInManager;
			_userManager = userManager;
			_emailSender = emailSender;
			_mapper = mapper;
		}

		public IActionResult AccessDenied()
		{
			return View();
		}

		[HttpGet]
		public IActionResult ForgotPassword()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ForgotPassword(ForgotPasswordModel forgotPasswordModel)
		{
			if (!ModelState.IsValid)
			{
				return View(forgotPasswordModel);
			}

			var user = await _userManager.FindByEmailAsync(forgotPasswordModel.Email);

			if (user == null)
			{
				ModelState.AddModelError("User not found", "Cannot found user email");
				return View(forgotPasswordModel);
			}
			else
			{
				var token = await _userManager.GeneratePasswordResetTokenAsync(user);
				var callback = Url.Action(nameof(ResetPassword), "Auth", new { token, email = user.Email }, Request.Scheme);
				var message = new Message(
					new string[] { user.Email },
					"Reset password",
					EmailTemplate.ForgotPasswordEmail(callback, user.Name),
					null);
				await _emailSender.SendEmailAsync(message);
				return RedirectToAction(nameof(ForgotPasswordConfirmation));
			}
		}
		public IActionResult ForgotPasswordConfirmation()
		{
			return View();
		}

		[HttpGet]
		public IActionResult ResetPassword(string token, string email)
		{
			var model = new ResetPasswordModel { Token = token, Email = email };
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ResetPassword(ResetPasswordModel resetPasswordModel)
		{
			if (!ModelState.IsValid)
			{
				return View(resetPasswordModel);
			}

			var user = await _userManager.FindByEmailAsync(resetPasswordModel.Email);

			if (user == null)
			{
				return NotFound();
			}

			var resetPassResult = await _userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.Password);

			if (!resetPassResult.Succeeded)
			{
				foreach (var error in resetPassResult.Errors)
				{
					ModelState.TryAddModelError(error.Code, error.Description);
				}
				return View();
			}
			ViewBag.success = "Change password successfully!";
			return RedirectToAction(nameof(ResetPasswordConfirmation));
		}

		[HttpGet]
		public IActionResult ResetPasswordConfirmation()
		{
			return View();
		}

		[HttpGet]
		public async Task<IActionResult> Login(string? returnUrl = null)
		{
			LoginModel model = new LoginModel();

			returnUrl ??= Url.Content("~/");

			// Clear the existing external cookie to ensure a clean login process
			await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);


			model.ReturnUrl = returnUrl;

			return View(model);
		}



		[HttpPost]
		public async Task<IActionResult> Login(LoginModel loginModel)
		{
			loginModel.ReturnUrl ??= Url.Content("~/");

			if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(loginModel.Email, loginModel.Password, loginModel.RememberMe, lockoutOnFailure: false);
				if (result.Succeeded)
				{
					TempData["success"] = Messages.SuccessLoginMessage;
					var user = await _userManager.FindByEmailAsync(loginModel.Email);

					if (user != null)
					{
						var roles = await _userManager.GetRolesAsync(user);
						if (roles.Contains("Customer"))
						{
							// Chuyển hướng đến trang dành cho khách hàng
							return RedirectToAction("Index", "Home", new { area = "Customer" });
						}
						else if (roles.Contains("CarOwner"))
						{
							// Chuyển hướng đến trang dành cho chủ xe
							return RedirectToAction("Index", "Home", new { area = "CarOwner" });
						}
					}

					return LocalRedirect(loginModel.ReturnUrl);
				}
				else
				{
					ModelState.AddModelError(string.Empty, "Invalid login attempt.");
					TempData["error"] = "Invalid login attempt.";
					return View(loginModel);
				}
			}
			TempData["error"] = "Something went wrong!";
			return View(loginModel);
		}


		public async Task<IActionResult> Register(string? returnUrl = null)
		{
			RegisterModel model = new RegisterModel();

			returnUrl ??= Url.Content("~/");

			// Clear the existing external cookie to ensure a clean login process
			await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);


			model.ReturnUrl = returnUrl;

			return View(model);
		}

		[HttpPost]
		public async Task<IActionResult> Register(RegisterModel register)
		{
			if (ModelState.IsValid)
			{
				var user = new ApplicationUser();
				user.Name = register.Name;
				user.Email = register.Email;
				user.UserName = register.Email;
				user.PhoneNo = register.PhoneNumber;
				user.PhoneNumber = register.PhoneNumber;

				var result = await _userManager.CreateAsync(user, register.Password);

				if (result.Succeeded)
				{

					if (register.RoleSelected == AppRoles.Customer)
					{
						var addRoleResult = await _userManager.AddToRoleAsync(user, AppRoles.Customer);
					}

					if (register.RoleSelected == AppRoles.CarOwner)
					{
						var addRoleResult = await _userManager.AddToRoleAsync(user, AppRoles.CarOwner);
					}

					await _signInManager.SignInAsync(user, isPersistent: false);

					var userLoggedIn = await _userManager.FindByEmailAsync(user.Email);

					if (userLoggedIn != null)
					{
						var roles = await _userManager.GetRolesAsync(userLoggedIn);
						if (roles.Contains("Customer"))
						{
							// Chuyển hướng đến trang dành cho khách hàng
							return RedirectToAction("Index", "Home", new { area = "Customer" });
						}
						else if (roles.Contains("CarOwner"))
						{
							// Chuyển hướng đến trang dành cho chủ xe
							return RedirectToAction("Index", "Home", new { area = "CarOwner" });
						}
					}

					return LocalRedirect(register.ReturnUrl);
				}
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(string.Empty, error.Description);
				}
			}

			// If we got this far, something failed, redisplay form
			return View(register);
		}
	}
}
