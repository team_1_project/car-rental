﻿using AutoMapper;
using CarRental.Core.Models;
using CarRental.DataAccess.Infrastructure;
using CarRental.MVC.Areas.Accounts.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarRental.MVC.Areas.Accounts.Controllers
{
    [Area("Accounts")]
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _host;
        private readonly IUnitOfWork _unitOfWork;

        public ProfileController(
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            IMapper mapper,
            IWebHostEnvironment host,
            IUnitOfWork unitOfWork)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _mapper = mapper;
            _host = host;
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Edit()
        {
            var user = await _userManager.GetUserAsync(User);

            if (user == null)
            {
                return NotFound();
            }

            EditUserModel userEdit = new EditUserModel()
            {
                DateOfBirth = user.DateOfBirth,
                DetailAddress = user.DetailAddress ?? "",
                EmailAddress = user.Email,
                ImageUrlDriverLicense = user.DrivingLicense,
                NationalIdNo = user.NationalIDNo,
                PhoneNumber = user.PhoneNumber,
                Name = user.Name ?? "",
            };

            ViewBag.Cities = await _unitOfWork.GetRepository<Address>()
                   .GetQuery()
                   .OrderBy(a => a.City)
                   .Select(a => a.City)
                   .Distinct()
                   .ToListAsync();

            if (user.AddressId != null)
            {
                userEdit.Address = await _unitOfWork.GetRepository<Address>()
                    .GetById(user.AddressId);
            }

            return View(userEdit);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserModel editUser)
        {
            var user = await _userManager.GetUserAsync(User);

            if (user == null)
            {
                return NotFound();
            }

            if (editUser.ImageFileDriverLicense != null)
            {
                editUser.ImageUrlDriverLicense = UploadProfileImage(editUser.ImageFileDriverLicense);
            }

            var address = _unitOfWork.GetRepository<Address>()
                .FirstOrDefault(filter: x => editUser.Address != null
                && x.City == editUser.Address.City
                && x.District == editUser.Address.District
                && x.Ward == editUser.Address.Ward).Result;

            if (address != null)
            {
                user.Address = address;
                user.AddressId = address.Id;
            }

            user.Name = editUser.Name;
            user.PhoneNumber = editUser.PhoneNumber;
            user.NationalIDNo = editUser.NationalIdNo;
            user.DetailAddress = editUser.DetailAddress;
            user.DateOfBirth = editUser.DateOfBirth;
            user.Email = editUser.EmailAddress;
            user.DrivingLicense = editUser.ImageUrlDriverLicense;


            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                TempData["success"] = "Edit user successfully!";
            }
            else
            {
                TempData["error"] = "Edit user failure!";
            }

            ViewBag.Cities = await _unitOfWork.GetRepository<Address>()
                   .GetQuery()
                   .OrderBy(a => a.City)
                   .Select(a => a.City)
                   .Distinct()
                   .ToListAsync();

            return RedirectToAction(nameof(Edit));
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);

                if (user != null)
                {
                    var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        // Đăng xuất người dùng
                        await _signInManager.SignOutAsync();
                        TempData["success"] = "Changed password successfully! Please sign in again.";

                        return Json(new
                        {
                            success = true,
                            redirect = Url.Action("Login", "Auth", new { area = "Accounts" }),
                        });
                    }
                    else
                    {
                        // Xử lý lỗi từ UserManager
                        var errorList = result.Errors.Select(e => e.Description).ToList();
                        return Json(new { success = false, errors = errorList });
                    }
                }
            }

            // Trả về lỗi ModelState nếu có
            var modelErrors = ModelState.Values
            .SelectMany(v => v.Errors)
            .Select(e => e.ErrorMessage)
            .ToList();

            return Json(new { success = false, errors = modelErrors });
        }

        private string UploadProfileImage(IFormFile? file)
        {
            string url = "";

            if (file != null)
            {
                var root = _host.WebRootPath;
                var carImagesPath = "profile-images";
                var prefix = Guid.NewGuid().ToString().ToUpper().Split('-')[0];
                var filePath = Path.Combine(root, carImagesPath, $"{prefix}-{file.FileName}");
                using (var stream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(stream);
                }

                url = $"/{carImagesPath}/{prefix}-{file.FileName}";
            }

            return url;
        }
    }
}
