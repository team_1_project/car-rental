﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CarRental.MVC.Areas.CarOwner.Models
{
    public class ReportViewModel
    {
        public int Ratings { get; set; }
        public int? MaxRate { get; set; } = 5;

        [StringLength(255)]
        public string Content { get; set; }

        public DateTime DateTime { get; set; }

        public virtual Booking Booking { get; set; }

        [ForeignKey(nameof(Booking))]
        public Guid BookingId { get; set; }

        public DateTime CreatedAt { get; set; }

        public string? CarImage { get; set; }
    }
}
