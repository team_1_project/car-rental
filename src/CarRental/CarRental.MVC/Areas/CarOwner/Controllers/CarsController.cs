﻿using AutoMapper;
using CarRental.Core.Common;
using CarRental.Core.Models;
using CarRental.Core.ViewModels;
using CarRental.DataAccess.Infrastructure;
using CarRental.DataAccess.Repositories;
using CarRental.MVC.Areas.CarOwner.Models;
using DocumentFormat.OpenXml.InkML;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;



namespace CarRental.MVC.Areas.CarOwner.Controllers
{
    [Area("CarOwner")]
    [Authorize(Roles = "CarOwner")]
    public class CarsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _host;
        private readonly ICarRepository _carRepository;

        public CarsController(
            IUnitOfWork unitOfWork,
            UserManager<ApplicationUser> userManager,
            IMapper mapper,
            IWebHostEnvironment host)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
            _host = host;
            _carRepository = _unitOfWork.GetCarRepository();
        }

        public async Task<IActionResult> Report(string filter = "All")
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var feedbacks = await _carRepository.GetFeedbacksByStar(filter, currentUser);

            var reports = _mapper.Map<List<ReportViewModel>>(feedbacks);
            for (int i = 0; i < feedbacks.Count; i++)
            {
                reports[i].CarImage = feedbacks[i].Booking.Car.CarImages.First(x => x.Description == "right").Url;
            }
            if (reports.Count != 0)
            {
                var averageRating = reports.Select(x => x.Ratings).Average();
                ViewBag.Stars = ((int)averageRating);
            }
            ViewBag.Filter = filter;
            ViewBag.AverageRating = _carRepository.GetAverageStarForUser(currentUser).ToString("#,#");

            return View(reports);
        }

        [HttpPost]
        public async Task<IActionResult> StopRenting(string carId)
        {
            var car = await _unitOfWork.GetRepository<Car>()
                .FirstOrDefaultInclude(x => x.Id == Guid.Parse(carId), includeProps: "Bookings");

            if (car != null)
            {
                if (car.Bookings.Any(x => x.Status != BookingStatus.Completed))
                {
                    TempData["error"] = "Stop renting car failled! Car have at least one booking hasn't finished.";
                    return Json(new { success = true, message = "Car have at least one booking hasn't finished." });
                }
                else
                {
                    car.IsStopRent = true;

                    // Lưu thay đổi vào cơ sở dữ liệu
                    _unitOfWork.GetRepository<Car>().Update(car);
                    await _unitOfWork.SaveChanges();
                    TempData["success"] = "Stop renting car sucessfully!";

                    // Trả về một thông báo hoặc dữ liệu JSON nếu cần
                    return Json(new { success = true, message = "Car rental stopped successfully." });
                }
            }

            // Trả về một thông báo hoặc dữ liệu JSON nếu có lỗi
            return Json(new { success = false, message = "Car not found." });
        }

        [HttpPost]
        public async Task<IActionResult> ContinueAvailable(string carId)
        {
            var car = await _unitOfWork.GetRepository<Car>()
                .FirstOrDefaultInclude(x => x.Id == Guid.Parse(carId), includeProps: "Bookings");

            if (car != null)
            {

                car.IsStopRent = false;

                // Lưu thay đổi vào cơ sở dữ liệu
                _unitOfWork.GetRepository<Car>().Update(car);
                await _unitOfWork.SaveChanges();
                TempData["success"] = "Continue available car sucessfully!";

                // Trả về một thông báo hoặc dữ liệu JSON nếu cần
                return Json(new { success = true, message = "Continue availble car sucessfully!" });
            }

            // Trả về một thông báo hoặc dữ liệu JSON nếu có lỗi
            return Json(new { success = false, message = "Car not found." });
        }



        // GET: CarOwner/Cars
        public async Task<IActionResult> Index()
        {
            // Get the current user
            var currentUser = await _userManager.GetUserAsync(User);

            if (currentUser == null)
            {
                // User is not authenticated
                return RedirectToAction("Login", "Account"); // Redirect to login page or handle as needed
            }
            var cars = await _unitOfWork
                .GetRepository<Car>()
                .GetQuery()
                .Where(x => x.CarOwnerId == currentUser.Id)
                .Include(x => x.Address)
                .Include(x => x.CarOwner)
                .Include(x => x.CarImages)
                .Include(x => x.Bookings)
                .ThenInclude(x => x.Feedback)
                .ToListAsync();
            return View(cars);
        }



        /// <summary>
        /// Sorting cars
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SortCars(string selectedOption, int page = 1, int pageSize = 3)
        {
            var currentUser = await _userManager.GetUserAsync(User);



            IEnumerable<CarRental.Core.Models.Car> sortedCars = null;

            // Depending on the selected option, sort the cars
            if (selectedOption == "Newest to lastest")
            {
                sortedCars = await _unitOfWork
                    .GetRepository<Car>()
                    .GetQuery()
                    .Where(x => x.CarOwnerId == currentUser.Id) // Compare with currentUser.Id
                    .OrderByDescending(x => x.CreatedAt)
                    .Include(x => x.Address)
                    .Include(x => x.CarOwner)
                    .Include(x => x.CarImages)
                    .Include(x => x.Bookings)
                    .ThenInclude(x => x.Feedback)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync();
            }
            else if (selectedOption == "Lastest to newest")
            {
                sortedCars = await _unitOfWork
                    .GetRepository<Car>()
                    .GetQuery()
                    .Where(x => x.CarOwnerId == currentUser.Id)
                    .OrderByDescending(x => x.ProductionYear) // Compare with currentUser.Id
                    .Include(x => x.CarOwner)
                    .Include(x => x.CarImages)
                    .Include(x => x.Address)
                    .Include(x => x.Bookings)
                    .ThenInclude(x => x.Feedback)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync();
            }



            // Render a partial view with the sorted data
            return PartialView("_CarListPartial", sortedCars);
        }


        public IActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CarViewModel carViewModel)
        {
            var currentUser = await _userManager.GetUserAsync(User);

            var car = await _carRepository.CreateCar(carViewModel, currentUser, _mapper);

            if (car == null)
            {
                return NotFound();
            }

            List<CarImage> carImages = new List<CarImage>()
            {
                UploadCarImage(carViewModel.ImageFileUploadCertificate,CarImagePosition.Certificate, car),
                UploadCarImage(carViewModel.ImageFileUploadRegistration,CarImagePosition.Registration, car),
                UploadCarImage(carViewModel.ImageFileUploadInsurance,CarImagePosition.Insurance, car),
                UploadCarImage(carViewModel.ImageFileUploadFront,CarImagePosition.Front, car),
                UploadCarImage(carViewModel.ImageFileUploadBack,CarImagePosition.Back, car),
                UploadCarImage(carViewModel.ImageFileUploadLeft,CarImagePosition.Left, car),
                UploadCarImage(carViewModel.ImageFileUploadRight,CarImagePosition.Right, car),
            };

            await _unitOfWork.GetRepository<Car>().Insert(car);
            await _unitOfWork.GetRepository<CarImage>().Insert(carImages.ToArray());
            await _unitOfWork.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Route("CarOwner/Cars/Edit/{carId:guid}")]
        public async Task<IActionResult> Edit(Guid carId)
        {
            var car = await _unitOfWork.GetRepository<Car>()
                .FirstOrDefaultInclude(filter: x => x.Id == carId,
                    includeProps: "Address,CarOwner,BrandModel,CarImages,Bookings");

            var query = _unitOfWork.GetRepository<Car>().GetQuery()
                           .Include(x => x.CarImages)
                           .Include(x => x.Address)
                           .Include(x => x.Bookings)
                           .ThenInclude(x => x.Feedback)
                           .FirstOrDefault(x => x.Id == carId);

            var bookingCount = _unitOfWork.GetRepository<Booking>()
                .GetQuery()
                .Count(b => b.CarId == carId);

            int ratingSum = query.Bookings.Sum(booking => booking.Feedback?.Ratings ?? 0);
            int countRatings = query.Bookings.Count(booking => booking.Feedback != null);
            double avgRate = countRatings > 0 ? (double)ratingSum / countRatings : 0;


            ViewBag.TotalRatings = ratingSum;
            ViewBag.RateCount = countRatings;
            ViewBag.AvgRate = avgRate;
            ViewBag.BookingCount = bookingCount;

            if (car == null)
            {
                return NotFound();
            }

            EditCarViewModel carViewModel = _mapper.Map<EditCarViewModel>(car);
            carViewModel.ImageUrlCertificate = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Certificate)?.Url;
            carViewModel.ImageUrlInsurance = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Insurance)?.Url;
            carViewModel.ImageUrlRegistration = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Registration)?.Url;
            carViewModel.ImageUrlBack = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Back)?.Url;
            carViewModel.ImageUrlFront = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Front)?.Url;
            carViewModel.ImageUrlLeft = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Left)?.Url;
            carViewModel.ImageUrlRight = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Right)?.Url;
            carViewModel.AdditionalFunctions = car.AdditionalFunction?.Split(",").ToList();
            carViewModel.TermsOfUsers = car.TermsOfUser?.Split(",").ToList();
            if (carViewModel.TermsOfUsers != null)
            {
                carViewModel.TermsOfUsers.ForEach(x =>
                {
                    if (x.Contains("Other"))
                    {
                        carViewModel.OtherSpecify = x.Skip(6).ToString();
                    }
                });
            }

            ViewBag.Cities = await _unitOfWork.GetRepository<Address>()
                   .GetQuery()
                   .OrderBy(a => a.City)
                   .Select(a => a.City)
                   .Distinct()
                   .ToListAsync();

            ViewBag.carId = car.Id;

            return View(carViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditPrice(EditCarViewModel carViewModel)
        {
            var result = await _carRepository.EditPrice(carViewModel);

            if (result)
            {
                TempData["success"] = "Edit car successfully!";
                return RedirectToAction(nameof(Index));
            }

            TempData["error"] = "Edit car failed!";
            return View(carViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditDetail(EditCarViewModel carViewModel)
        {
            var car = await _carRepository.EditDetail(carViewModel);

            if (car == null)
            {
                return NotFound();
            }

            await EditUploadCarImage(carViewModel.ImageFileUploadBack, CarImagePosition.Back, car);
            await EditUploadCarImage(carViewModel.ImageFileUploadFront, CarImagePosition.Front, car);
            await EditUploadCarImage(carViewModel.ImageFileUploadLeft, CarImagePosition.Left, car);
            await EditUploadCarImage(carViewModel.ImageFileUploadRight, CarImagePosition.Right, car);

            _unitOfWork.GetRepository<Car>().Update(car);
            await _unitOfWork.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        private async Task EditUploadCarImage(IFormFile? file, string position, Car car)
        {
            if (file != null)
            {
                var carImage = await _unitOfWork.GetRepository<CarImage>()
                    .FirstOrDefault(x => x.CarId == car.Id && x.Description == position);

                if (carImage != null)
                {
                    _unitOfWork.GetRepository<CarImage>().HardDelete(carImage.Id);
                    var newCarImage = UploadCarImage(file, position, car);
                    await _unitOfWork.GetRepository<CarImage>().Insert(newCarImage);
                }
            }
        }


        private CarImage UploadCarImage(IFormFile? file, string position, Car car)
        {
            string url = "";

            if (file != null)
            {
                var root = _host.WebRootPath;
                var carImagesPath = "car-images";
                var prefix = Guid.NewGuid().ToString().ToUpper().Split('-')[0];
                var filePath = Path.Combine(root, carImagesPath, $"{prefix}-{file.FileName}");
                using (var stream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(stream);
                }

                url = $"/{carImagesPath}/{prefix}-{file.FileName}";
            }

            CarImage carImage = new CarImage()
            {
                Url = url,
                Description = position,
                CarId = car.Id,
                Car = car
            };

            return carImage;
        }
    }

}