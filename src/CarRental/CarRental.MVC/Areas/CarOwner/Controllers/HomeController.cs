﻿using CarRental.Core.Common;
using CarRental.DataAccess.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.MVC.Areas.CarOwner.Controllers
{
    [Area("CarOwner")]
    [Authorize(Roles = AppRoles.CarOwner)]
    public class HomeController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        
    }
}
