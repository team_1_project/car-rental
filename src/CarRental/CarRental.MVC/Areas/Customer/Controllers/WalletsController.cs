﻿using AutoMapper;
using CarRental.Core.Common;
using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Infrastructure;
using CarRental.MVC.Areas.Customer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarRental.MVC.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class WalletsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _autoMapper;

        public WalletsController(IUnitOfWork unitOfWork
                                , ApplicationDbContext dbContext
                                , UserManager<ApplicationUser> userManager
                                , IMapper autoMapper
                                )
        {
            this._unitOfWork = unitOfWork;
            this._dbContext = dbContext;
            this._userManager = userManager;
            this._autoMapper = autoMapper;
        }

        public async Task<ApplicationUser> GetCurrentUser()
        {
            return await _userManager.GetUserAsync(User);
        }
        public async Task<IActionResult> Index()
        {
            var currentUser = await GetCurrentUser();

            var walletTransaction = await _unitOfWork.GetRepository<WalletTransactionHistory>()
                .GetQuery()
                .Include(x => x.User)
                .Where(x => x.UserId == currentUser.Id)
                .OrderByDescending(x => x.CreatedAt)
                .ToListAsync();
            ViewBag.currentBalance = currentUser.Wallet;

            return View(walletTransaction);
        }

        [HttpPost]
        public async Task<ActionResult> TopUpAction(decimal topUpAmount)
        {
            try
            {
                var currentUser = await GetCurrentUser();
                currentUser.Wallet += topUpAmount;
                var transactionCustomer = new WalletTransactionHistory()
                {
                    UserId = currentUser.Id,
                    User = currentUser,
                    Amount = topUpAmount,
                    TransactionType = TransactionType.TopUp,
                    TransactionDescription = "Topup into wallet"
                };
                await _unitOfWork.GetRepository<WalletTransactionHistory>().Insert(transactionCustomer);
                await _unitOfWork.SaveChanges();

                // Redirect to the Customer/Wallets page
                return RedirectToAction("Index", "Wallets", new { area = "Customer" });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Wallets", new { area = "Customer" });
            }
        }

        [HttpPost]
        public async Task<ActionResult> WithdrawAction(decimal withdrawAmount)
        {
            try
            {
                var currentUser = await GetCurrentUser();

                // Perform validation or any other necessary checks here

                if (withdrawAmount <= 0)
                {
                    return Json(new { success = false, message = "Invalid withdrawal amount" });
                }

                if (withdrawAmount > currentUser.Wallet)
                {
                    return Json(new { success = false, message = "Insufficient balance" });
                }

                // Perform the withdrawal
                currentUser.Wallet -= withdrawAmount;
                var transactionCustomer = new WalletTransactionHistory()
                {
                    UserId = currentUser.Id,
                    User = currentUser,
                    Amount = withdrawAmount,
                    TransactionType = TransactionType.WithDraw,
                    TransactionDescription = "Withdraw from wallet"
                };
                await _unitOfWork.GetRepository<WalletTransactionHistory>().Insert(transactionCustomer);
                await _unitOfWork.SaveChanges();

                return RedirectToAction("Index", "Wallets", new { area = "Customer" });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Wallets", new { area = "Customer" });
            }
        }

    }
}
