﻿using AutoMapper;
using CarRental.Core.Common;
using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.Core.ViewModels;
using CarRental.DataAccess.Infrastructure;
using CarRental.MVC.Areas.Accounts.Models;
using CarRental.MVC.Areas.Customer.Models;
using CarRental.MVC.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Globalization;

namespace CarRental.MVC.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize(Roles = AppRoles.Customer)]
    public class CarsController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly Accounts.Models.IEmailSender _emailSender;
        private readonly IMapper _mapper;

        public CarsController(IUnitOfWork unitOfWork, ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment, Accounts.Models.IEmailSender emailSender, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            this.dbContext = dbContext;
            _emailSender = emailSender;
            _mapper = mapper;
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> SearchCar(string location, DateTime pickupDateTime, DateTime dropOffDateTime)
        {
            var query = dbContext.Cars
                .Include(x => x.Address)
                .Include(x => x.CarImages)
                .Include(x => x.Bookings)
                .ThenInclude(x => x.Feedback)
                .AsEnumerable();

            if (location != null)
            {
                query = query.Where(x => CultureInfo.CurrentCulture.CompareInfo
                .IndexOf(x.FullText, location, CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace) >= 0);
            }
            if (pickupDateTime != DateTime.MinValue && dropOffDateTime != DateTime.MinValue)
            {
                query = query.Where(car => car.IsActive && car.IsStopRent == false && car.Bookings.All(booking =>
                    booking.StartDateTime > dropOffDateTime || booking.EndDateTime.AddDays(1) < pickupDateTime));
            }

            double rentalDays = (dropOffDateTime - pickupDateTime).TotalDays + 1;

            TempData["pickUpDatetime"] = pickupDateTime.ToString();
            TempData["dropOffDateTime"] = dropOffDateTime.ToString();
            TempData["rentalDays"] = rentalDays.ToString();

            ViewBag.Location = location;
            ViewBag.PickupDate = pickupDateTime.ToString("yyyy-MM-ddTHH:mm");
            ViewBag.DropOffDate = dropOffDateTime.ToString("yyyy-MM-ddTHH:mm");

            return View(query.ToList());
        }

        [HttpGet]
        [Route("Customer/Cars/RentCar/{carID:guid}")]
        public async Task<IActionResult> RentCar(Guid carID, string fulltext)
        {
            var startTime = TempData["pickUpDatetime"];
            var endTime = TempData["dropOffDateTime"];
            // Generate a random distinct 4-digit number
            Random rand = new Random();
            string randomNumber;
            do
            {
                randomNumber = rand.Next(1000, 10000).ToString();
            } while (IsBookingNumberExists(randomNumber));

            // Create the booking number in the desired format
            string bookingYearMonth = DateTime.Now.ToString("yyyyMMdd");
            string bookingNo = $"{bookingYearMonth}{randomNumber}";
            TempData["BookingNo"] = bookingNo;
            TempData["carID"] = carID;
            double rentalDays = double.Parse(TempData["rentalDays"] as string);
            var currentUser = await _userManager.GetUserAsync(User);
            var userEmail = currentUser.Email;
            var userName = currentUser.Name;
            var userNational = currentUser.NationalIDNo;
            var userWallet = currentUser.Wallet;
            var userPhoneNo = currentUser.PhoneNumber;
            var user = currentUser;
            var DOB = currentUser.DateOfBirth;

            if (currentUser.AddressId == null)
            {
                TempData["error"] = "Please fill your profile before rent a car";
                return Redirect("/Accounts/Profile/Edit?username=" + currentUser.UserName);
            }

            var car = dbContext.Cars
                            .Include(x => x.CarImages)
                            .Include(x => x.Address)
                            .Include(x => x.Bookings)
                            .ThenInclude(x => x.Feedback)
                            .FirstOrDefault(x => x.Id == carID);

            int ratingSum = car.Bookings.Sum(booking => booking.Feedback?.Ratings ?? 0);
            int countRatings = car.Bookings.Count(x => x.Feedback != null);
            double avgRate = (double)ratingSum / countRatings;

            var bookingCount = dbContext.Bookings
                .Where(b => b.CarId == carID)
                .Count();

            var cities = (await _unitOfWork.GetRepository<Address>().GetAll())
                            .Select(address => new { City = address.City, CityCode = address.CityCode })
                            .Distinct()
                            .ToList();

            ViewBag.TotalRatings = ratingSum;
            ViewBag.RateCount = countRatings;
            ViewBag.AvgRate = avgRate;
            ViewBag.User = user;
            ViewBag.FullText = fulltext;
            ViewBag.CarID = carID;
            ViewBag.City = cities;
            ViewBag.Car = car;
            ViewBag.Email = userEmail;
            ViewBag.PhoneNo = userPhoneNo;
            ViewBag.Name = userName;
            ViewBag.DOB = DOB;
            ViewBag.NationalID = userNational;
            ViewBag.Wallet = userWallet;
            ViewBag.BookingCount = bookingCount;
            ViewBag.BasePrice = car.BasePrice;
            ViewBag.BookingNo = bookingNo;
            ViewBag.PickUpDate = TempData["pickUpDatetime"];
            ViewBag.DropOffDate = TempData["dropOffDateTime"];
            ViewBag.TotalDays = rentalDays;
            ViewBag.TotalPrice = (decimal)rentalDays * car.BasePrice;
            ViewBag.TotalDeposit = (decimal)rentalDays * car.BasePrice + car.Deposit;
            ViewBag.CarImages = car.CarImages;
            ViewBag.FrontImage = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Front);
            ViewBag.BackImage = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Back);
            ViewBag.LeftImage = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Left);
            ViewBag.RightImage = car.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Right);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RentCarPost(BookingViewModel bookingViewModel, string PickUpDate, string DropOffDate, string BookingNo, string CarId, string TotalDeposit)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var car = _unitOfWork.GetRepository<Car>().GetQuery().Include(x => x.CarOwner).FirstOrDefault(x => x.Id == Guid.Parse(CarId));

            var booking = _mapper.Map<Booking>(bookingViewModel);
            booking.BookingNo = BookingNo;
            booking.StartDateTime = Convert.ToDateTime(PickUpDate);
            booking.EndDateTime = Convert.ToDateTime(DropOffDate);
            booking.Customer = currentUser;
            booking.CustomerId = currentUser.Id;
            booking.Car = car;
            booking.CarId = car.Id;
            booking.Status = BookingStatus.Confirmed;

            // Check if a file was uploaded for DriverDrivingLicenseImage
            if (bookingViewModel.DriverDrivingLicenseImage != null && bookingViewModel.DriverDrivingLicenseImage.Length > 0)
            {
                var fileName = Path.GetFileName(bookingViewModel.DriverDrivingLicenseImage.FileName);
                var fileExtension = Path.GetExtension(fileName).ToLower(); // Get the file extension and convert it to lowercase
                var allowedExtensions = new string[] { ".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".docx" }; // Define the allowed file extensions

                if (allowedExtensions.Contains(fileExtension))
                {
                    var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "profile-images", fileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await bookingViewModel.DriverDrivingLicenseImage.CopyToAsync(stream);
                    }

                    booking.DriverDrivingLicense = "/profile-images/" + fileName;
                }
                else
                {
                    ModelState.AddModelError("DriverDrivingLicenseImage", "Invalid file type. Please upload an image (jpg, jpeg, png, gif) or a document (pdf, doc, docx).");
                }
            }

            if (Convert.ToDecimal(TotalDeposit) > currentUser.Wallet)
            {
                ModelState.AddModelError("", "Wallet balance is insufficient. Please recharge your wallet before submitting the form.");
            }
            else
            {
                // Add booking :
                await _unitOfWork.GetRepository<Booking>().Insert(booking);

                //Substract customer wallet for car deposit and Log customer transaction:
                currentUser.Wallet -= Convert.ToDecimal(TotalDeposit);
                var transactionCustomerHistory = new WalletTransactionHistory()
                {
                    UserId = currentUser.Id,
                    User = currentUser,
                    Amount = Convert.ToDecimal(TotalDeposit),
                    TransactionType = TransactionType.Deposit,
                    TransactionDescription = "Transfer car deposit for car's owner"
                };

                //Receive deposit from customer and  Log car owner transaction
                car.CarOwner.Wallet += Convert.ToDecimal(TotalDeposit);
                var transactionCarOwnerHistory = new WalletTransactionHistory()
                {
                    UserId = car.CarOwnerId,
                    User = car.CarOwner,
                    Amount = Convert.ToDecimal(TotalDeposit),
                    TransactionType = TransactionType.ReceiveDeposit,
                    TransactionDescription = "Receive deposit car from customer"
                };
                await _unitOfWork.GetRepository<WalletTransactionHistory>().Insert(transactionCustomerHistory);
                await _unitOfWork.GetRepository<WalletTransactionHistory>().Insert(transactionCarOwnerHistory);
                await _unitOfWork.SaveChanges();

                // Send Email to Customers about booking;
                var bookingHasBought = await _unitOfWork.GetRepository<Booking>().FirstOrDefault(x => x.BookingNo.Equals(BookingNo));
                var message = new Message
                (
                    new string[] { currentUser.Email },
                    "Confirmed booking",
                    EmailTemplate.ConfirmedBooking(bookingHasBought, BookingNo, currentUser.Name)
                );
                await _emailSender.SendEmailAsync(message, isHtml: true);

                //Send Email to Car Owner about received deposit from customer
                var carOwnerMail = car.CarOwner.Email;
                var messageForCarOwner = new Message
                (
                    new string[] { car.CarOwner.Email },
                    "Received deposit from customer",
                    EmailTemplate.ReceivedDepositFromCustomer(TotalDeposit, car.Name, BookingNo, car.CarOwner.Name)
                );
                await _emailSender.SendEmailAsync(messageForCarOwner, isHtml: true);
            }

            return Redirect("/Customer/Bookings/Index");
        }

        [HttpGet]
        public IActionResult GetDistricts(string cityCode)
        {
            var districts = _unitOfWork.GetRepository<Address>()
                .GetQuery(x => x.CityCode.Contains(cityCode))
                .Select(address => address.District)
                .Distinct()
                .ToList();

            var data = new
            {
                districts = districts
            };

            return Json(data);
        }

        [HttpGet]
        public IActionResult GetWards(string cityCode, string district)
        {
            var wards = _unitOfWork.GetRepository<Address>()
                .GetQuery(x => x.CityCode.Contains(cityCode) && x.District == district)
                .Select(address => address.Ward)
                .Distinct()
                .ToList();

            var data = new
            {
                wards = wards
            };

            return Json(data);
        }

        private bool IsBookingNumberExists(string bookingNumber)
        {
            // Check if any booking in the database has the given booking number
            bool exists = dbContext.Bookings.Any(b => b.BookingNo == bookingNumber);
            return exists;
        }

        [HttpPost]
        public async Task<IActionResult> SortCars(string selectedOption, int page = 1, int pageSize = 3)
        {

            IQueryable<Car> sortedQuery = _unitOfWork.GetRepository<Car>().GetQuery();

            if (selectedOption == "Newest to Oldest")
            {
                sortedQuery = sortedQuery.OrderByDescending(x => x.CreatedAt);
            }
            else if (selectedOption == "Oldest to Newest")
            {
                sortedQuery = sortedQuery.OrderBy(x => x.CreatedAt);
            }

            var sortedCars = await sortedQuery
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return PartialView("_SearchResultsPartial", sortedCars);
        }

        [HttpGet]
        [Route("Customer/Cars/Detail/{carId:guid}/{fulltext:?}")]
        public async Task<IActionResult> Detail(Guid carId, string fulltext)
        {
            ViewBag.FullText = fulltext;
            var rs = await _unitOfWork.GetRepository<Car>()
                .FirstOrDefaultInclude(filter: x => x.Id == carId,
                    includeProps: "BrandModel,CarImages,Bookings");

            //rating
            var query = _unitOfWork.GetRepository<Car>().GetQuery()
                           .Include(x => x.CarImages)
                           .Include(x => x.Address)
                           .Include(x => x.Bookings)
                           .ThenInclude(x => x.Feedback)
                           .FirstOrDefault(x => x.Id == carId);

            if (query != null)
            {
                int ratingSum = query.Bookings?.Sum(booking => booking.Feedback?.Ratings ?? 0) ?? 0;
                int countRatings = query.Bookings?.Count(x => x.Feedback != null) ?? 0;
                double avgRate = countRatings > 0 ? (double)ratingSum / countRatings : 0;

                ViewBag.TotalRatings = ratingSum;
                ViewBag.RateCount = countRatings;
                ViewBag.AvgRate = avgRate;
            }

            if (rs == null)
            {
                return NotFound();
            }
            

            CustomerCarViewModel customerCarViewModel = _mapper.Map<CustomerCarViewModel>(rs);
            customerCarViewModel.ImageUrlCertificate = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Certificate)?.Url;
            customerCarViewModel.ImageUrlInsurance = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Insurance)?.Url;
            customerCarViewModel.ImageUrlRegistration = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Registration)?.Url;
            customerCarViewModel.ImageUrlBack = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Back)?.Url;
            customerCarViewModel.ImageUrlFront = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Front)?.Url;
            customerCarViewModel.ImageUrlLeft = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Left)?.Url;
            customerCarViewModel.ImageUrlRight = rs.CarImages.FirstOrDefault(x => x.Description == CarImagePosition.Right)?.Url;
            return View(customerCarViewModel);
        }
    }
}