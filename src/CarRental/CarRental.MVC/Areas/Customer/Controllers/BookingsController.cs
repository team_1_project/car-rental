﻿using AutoMapper;
using CarRental.Core.Common;
using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Infrastructure;
using CarRental.MVC.Areas.Accounts.Models;
using CarRental.MVC.Areas.Customer.Models;
using CarRental.MVC.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarRental.MVC.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize(Roles = AppRoles.Customer)]
    public class BookingsController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly Accounts.Models.IEmailSender _emailSender;

        public BookingsController(
            IUnitOfWork unitOfWork,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            IMapper mapper,
            Accounts.Models.IEmailSender emailSender,
            IWebHostEnvironment webHostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _dbContext = dbContext;
            _userManager = userManager;
            _mapper = mapper;
            _emailSender = emailSender;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            var currentUser = await _userManager.GetUserAsync(User);

            if (currentUser == null)
            {
                return RedirectToAction("Login", "Account");
            }

            var bookings = await _unitOfWork
                .GetRepository<Booking>()
                .GetQuery()
                .Where(x => x.CustomerId == currentUser.Id)
                .Include(x => x.Customer)
                .Include(x => x.Car)
                .ThenInclude(x => x.CarImages)
                .ToListAsync();

            return View(bookings);
        }

        [HttpPost]
        public async Task<IActionResult> SortBookings(string selectedOption, int page = 1, int pageSize = 10)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            IEnumerable<CarRental.Core.Models.Booking> sortedBookings = null;

            var query = _unitOfWork
                .GetRepository<Booking>()
                .GetQuery()
                .Where(x => x.CustomerId == currentUser.Id)
                .Include(x => x.Customer)
                .Include(x => x.Car)
                .ThenInclude(x => x.CarImages);

            switch (selectedOption)
            {
                case "Latest to newest":
                    sortedBookings = await query
                        .OrderBy(x => x.CreatedAt)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToListAsync();
                    break;
                case "Price highest to lowest":
                    sortedBookings = await query
                        .OrderByDescending(x => x.Car.BasePrice)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToListAsync();
                    break;
                case "Price lowest to highest":
                    sortedBookings = await query
                        .OrderBy(x => x.Car.BasePrice)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToListAsync();
                    break;
                default:
                    sortedBookings = await query
                        .OrderByDescending(x => x.CreatedAt)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToListAsync();
                    break;
            }

            return PartialView("_BookingListPartial", sortedBookings);
        }



        [HttpPost]
        public async Task<IActionResult> CancelBooking(Guid bookingId)
        {
            DateTime cancelTime = DateTime.Now;
            var booking = await _dbContext.Bookings
                .Include(b => b.Customer)
                .Include(b => b.Car.CarOwner)
                .FirstOrDefaultAsync(b => b.Id == bookingId);

            if (booking == null)
            {
                return Json(new { success = false });
            }

            if (booking.Status == BookingStatus.Cancelled)
            {
                // The booking is already cancelled; handle this case as needed
                return RedirectToAction("Index", "Bookings", new { area = "Customer" });
            }

            // If user cancel after start date, return to bookings list and required user to confirm pick up
            if (cancelTime >= booking.StartDateTime)
            {
                return RedirectToAction("Index", "Bookings", new { area = "Customer" });
            }

            var customer = booking.Customer;
            var carOwner = booking.Car.CarOwner;
            var daysLeft = (booking.EndDateTime - booking.StartDateTime).TotalDays + 1;
            var totalPrice = (booking.Car.BasePrice * (decimal)daysLeft) + booking.Car.Deposit;

            // Update booking status to cancelled
            booking.Status = BookingStatus.Cancelled;
            _dbContext.Entry(booking).State = EntityState.Modified;

            // Perform financial transactions
            customer.Wallet += totalPrice;
            carOwner.Wallet -= totalPrice;

            var returnTotalPriceForCustomer = new WalletTransactionHistory()
            {
                User = customer,
                UserId = customer.Id,
                Amount = totalPrice,
                TransactionType = TransactionType.Refund,
                TransactionDescription = "Refund for customer from cancel booking"
            };
            var drawMoneyFromCarOwner = new WalletTransactionHistory()
            {
                User = carOwner,
                UserId = carOwner.Id,
                Amount = totalPrice,
                TransactionType = TransactionType.WithDraw,
                TransactionDescription = "Withdraw from car owner from cancel booking"
            };

            await _unitOfWork.GetRepository<WalletTransactionHistory>().Insert(returnTotalPriceForCustomer, drawMoneyFromCarOwner);

            // Save changes to the database
            await _dbContext.SaveChangesAsync();

            // Send email notification to car owner
            var carOwnerMail = carOwner.Email;
            var messageForCarOwner = new Message(
                new string[] { carOwnerMail },
                "Your car has been canceled booking from customer",
                EmailTemplate.CancelBookingFromCustomer(booking.Car.Name, booking.BookingNo, booking.Car.CarOwner.Name)
            );
            await _emailSender.SendEmailAsync(messageForCarOwner, isHtml: true);

            return RedirectToAction("Index", "Bookings", new { area = "Customer" });
        }




        [HttpPost]
        public async Task<IActionResult> ConfirmPickup(Guid bookingId)
        {
            var booking = await _dbContext.Bookings.FirstOrDefaultAsync(b => b.Id == bookingId);
            if (booking != null)
            {
                booking.Status = BookingStatus.InProgress;
                _dbContext.Entry(booking).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();

                return RedirectToAction("Index");
            }
            return Json(new { success = false });
        }


        [Route("Customer/Bookings/Details/{bookingId:guid}/{carID:guid}")]
        public async Task<IActionResult> Details(Guid bookingId, Guid carID)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            currentUser.Address = await _unitOfWork.GetRepository<Address>().FirstOrDefault(x => x.Id == currentUser.AddressId);
            ViewBag.User = currentUser;

            var cities = (await _unitOfWork.GetRepository<Address>().GetAll())
                            .Select(address => new { City = address.City, CityCode = address.CityCode })
                            .Distinct()
                            .ToList();
            ViewBag.City = cities;

            // Lấy thông tin booking từ repository
            var booking = await _unitOfWork.GetRepository<Booking>()
                .GetQuery()
                .Include(x => x.Customer)
                .Include(x => x.Car)
                .ThenInclude(x => x.CarImages)
                .FirstOrDefaultAsync(x => x.Id == bookingId);

            //Lấy thông tin car
            var car = await _unitOfWork.GetRepository<Car>()
                .GetQuery()
                .Include(x => x.BrandModel)
                //.Include(x => x.TermsOfUser)
                .FirstOrDefaultAsync(x => x.Id == carID);

            var carBrand = car?.BrandModel?.Brand;
            var carModel = car?.BrandModel?.Model;

            ViewBag.CarBrand = carBrand;
            ViewBag.CarModel = carModel;
            ViewBag.Booking = booking;

            var termsOfUserString = string.Join(",", car?.TermsOfUser);

            ViewBag.TermsOfUser = termsOfUserString;


            // Đếm số lần đặt car từ Booking
            var bookingCarCount = _unitOfWork.GetRepository<Booking>()
                .GetQuery()
                .Count(x => x.CarId == carID);

            //Mapping data Booking -> BookingViewModel
            var bookingViewModel = _mapper.Map<BookingViewModel>(booking);
            bookingViewModel.CarImages = booking.Car.CarImages;
            bookingViewModel.BookingCarCount = bookingCarCount;

            return View(bookingViewModel);
        }


        [HttpPost]
        [Route("Customer/Bookings/Details/{bookingId:guid}/{carID:guid}")]
        public async Task<IActionResult> Details(Guid bookingId, Guid carID, BookingViewModel editBooking)
        {
            if (bookingId == Guid.Empty)
            {
                return NotFound();
            }

            var booking = await _unitOfWork.GetRepository<Booking>()
                .GetQuery()
                .FirstOrDefaultAsync(x => x.Id == bookingId);

            if (booking == null)
            {
                return NotFound();
            }

            // Cập nhật 
            booking.Name = editBooking.Name;
            booking.PhoneNo = editBooking.PhoneNo;
            booking.DateOfBirth = editBooking.DateOfBirth;
            booking.NationalIDNo = editBooking.NationalIDNo;
            booking.City = editBooking.City;
            booking.CityCode = editBooking.CityCode;
            booking.District = editBooking.District;
            booking.Ward = editBooking.Ward;
            booking.DetailAddress = editBooking.DetailAddress;

            booking.DriverName = editBooking.DriverName;
            booking.DriverPhoneNo = editBooking.DriverPhoneNo;
            booking.DriverDateOfBirth = editBooking.DriverDateOfBirth;
            booking.DriverNationalIDNo = editBooking.DriverNationalIDNo;
            booking.DriverCity = editBooking.DriverCity;
            booking.DriverDistrict = editBooking.DriverDistrict;
            booking.DriverWard = editBooking.DriverWard;
            booking.DriverDetailAddress = editBooking.DriverDetailAddress;

            // Check if a file was uploaded for DriverDrivingLicenseImage
            if (editBooking.DriverDrivingLicenseImage != null && editBooking.DriverDrivingLicenseImage.Length > 0)
            {
                var fileName = Path.GetFileName(editBooking.DriverDrivingLicenseImage.FileName);
                var fileExtension = Path.GetExtension(fileName).ToLower(); // Get the file extension and convert it to lowercase
                var allowedExtensions = new string[] { ".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".docx" }; // Define the allowed file extensions

                if (allowedExtensions.Contains(fileExtension))
                {
                    var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "profile-images", fileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await editBooking.DriverDrivingLicenseImage.CopyToAsync(stream);
                    }

                    booking.DriverDrivingLicense = "/profile-images/" + fileName;
                }
                else
                {
                    ModelState.AddModelError("DriverDrivingLicenseImage", "Invalid file type. Please upload an image (jpg, jpeg, png, gif) or a document (pdf, doc, docx).");
                }
            }
            if (editBooking.DrivingLicenseImage != null && editBooking.DrivingLicenseImage.Length > 0)
            {
                var fileName = Path.GetFileName(editBooking.DrivingLicenseImage.FileName);
                var fileExtension = Path.GetExtension(fileName).ToLower(); // Get the file extension and convert it to lowercase
                var allowedExtensions = new string[] { ".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".docx" }; // Define the allowed file extensions

                if (allowedExtensions.Contains(fileExtension))
                {
                    var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "profile-images", fileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await editBooking.DrivingLicenseImage.CopyToAsync(stream);
                    }

                    booking.DrivingLicense = "/profile-images/" + fileName;
                }
                else
                {
                    ModelState.AddModelError("DrivingLicenseImage", "Invalid file type. Please upload an image (jpg, jpeg, png, gif) or a document (pdf, doc, docx).");
                }
            }

            try
            {
                _unitOfWork.GetRepository<Booking>().Update(booking);
                await _unitOfWork.SaveChanges();
                ViewBag.success = "Booking details updated successfully!";
            }
            catch (Exception ex)
            {
                ViewBag.error = "Error updating booking details: " + ex.Message;
            }

            return RedirectToAction("Index");
        }

        [Route("Customer/Bookings/ReturnCar/{bookingId:guid}")]
        [HttpPost]
        public async Task<IActionResult> ReturnCar(Guid bookingId)
        {
            // Lấy thông tin của Booking dựa trên bookingId
            var booking = await _unitOfWork.GetRepository<Booking>()
                .GetQuery()
                .Include(x => x.Customer)
                .Include(x => x.Car)
                .ThenInclude(x => x.CarOwner)
                .FirstOrDefaultAsync(x => x.Id == bookingId);

            var user = await _userManager.GetUserAsync(User);
            var carOwner = await _userManager.FindByIdAsync(booking.Car.CarOwner.Id);

            if (booking == null || booking.Car == null || user == null || carOwner == null)
            {
                return NotFound();
            }

            // Tính toán số tiền cần trả dựa trên BasePrice và số ngày thuê xe
            DateTime currentDate = DateTime.Now;
            decimal totalAmountToPay = booking.Car.BasePrice * ((currentDate - booking.StartDateTime).Days + 1) + booking.Car.Deposit;
            decimal totalDeposit = booking.Car.BasePrice * ((booking.EndDateTime - booking.StartDateTime).Days + 1) + booking.Car.Deposit;

            bool offsetSuccess = await OffsetFinalPayment(booking, user, carOwner);

            if (offsetSuccess)
            {
                TempData["success"] = "Return to user wallet successfully!";
            }
            else
            {
                ViewBag.TotalAmountToPay = totalAmountToPay;

                if (totalDeposit > totalAmountToPay)
                {
                    TempData["success"] = "Deducted in user wallet successfully!";
                }
                else
                {
                    TempData["success"] = "Deducted in car owner wallet successfully!";
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Return true in normal case: total amount to pay less than total deposit, return 
        /// false when total amount more than total deposit
        /// </summary>
        /// <param name="booking"></param>
        /// <param name="user"></param>
        /// <param name="carOwner"></param>
        /// <returns>Return true in normal case: total amount to pay less than total deposit, return 
        /// false when total amount more than total deposit</returns>
        /// 

        [HttpPost]
        public async Task<IActionResult> RateCar(Feedback feedback, string bookingNo, int ratings)
        {
            var booking = _unitOfWork.GetRepository<Booking>().GetQuery()
                .FirstOrDefault(x => x.BookingNo == bookingNo);

            if (booking != null)
            {
                feedback.BookingId = booking.Id;
                feedback.Ratings = ratings;
                if (feedback.Ratings >= 0 && feedback.Ratings <= feedback.MaxRate)
                {
                    // add feedback to the database
                    await _unitOfWork.GetRepository<Feedback>().Insert(feedback);
                    await _unitOfWork.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    // return bad request for an invalid rating value
                    return BadRequest("Invalid ratings value");
                }

            }
            return Json(new { success = false });
        }
        private async Task<bool> OffsetFinalPayment(Booking booking, ApplicationUser user, ApplicationUser carOwner)
        {
            DateTime currentDate = DateTime.Now;
            decimal totalAmountToPay = booking.Car.BasePrice * ((currentDate - booking.StartDateTime).Days + 1) + booking.Car.Deposit;
            decimal totalDeposit = booking.Car.BasePrice * ((booking.EndDateTime - booking.StartDateTime).Days + 1) + booking.Car.Deposit;

            if (totalAmountToPay > totalDeposit)
            {
                var walletHistoryForCustomer = new WalletTransactionHistory()
                {
                    User = user,
                    UserId = user.Id,
                    TransactionDate = DateTime.Now,
                    Amount = (totalAmountToPay - totalDeposit), // Set the amount for customer's transaction
                    TransactionType = TransactionType.WithDraw,
                    TransactionDescription = "Offset final payment in customer wallet!"
                };
                var walletHistoryForCustomerDeposit = new WalletTransactionHistory()
                {
                    User = user,
                    UserId = user.Id,
                    TransactionDate = DateTime.Now,
                    Amount = booking.Car.Deposit, // Set the amount for customer's transaction
                    TransactionType = TransactionType.TopUp,
                    TransactionDescription = "Offset final payment deposit in customer wallet!"
                };

                var walletHistoryForCarOwner = new WalletTransactionHistory()
                {
                    User = carOwner,
                    UserId = carOwner.Id,
                    TransactionDate = DateTime.Now,
                    Amount = (totalAmountToPay - totalDeposit), // Set the amount for car owner's transaction
                    TransactionType = TransactionType.TopUp,
                    TransactionDescription = "Offset final payment in car owner wallet!"
                };
                var walletHistoryForCarOwnerDeposit = new WalletTransactionHistory()
                {
                    User = carOwner,
                    UserId = carOwner.Id,
                    TransactionDate = DateTime.Now,
                    Amount = booking.Car.Deposit, // Set the amount for car owner's transaction
                    TransactionType = TransactionType.WithDraw,
                    TransactionDescription = "Offset final payment deposit in car owner wallet!"
                };

                user.Wallet -= (totalAmountToPay - totalDeposit);
                user.Wallet += booking.Car.Deposit;

                carOwner.Wallet += (totalAmountToPay - totalDeposit);
                carOwner.Wallet -= booking.Car.Deposit;

                booking.Status = BookingStatus.Completed;
                booking.EndDateTime = DateTime.Now;

                _unitOfWork.GetRepository<Booking>().Update(booking);

                // Insert wallet transaction history records for customer and car owner
                await _unitOfWork.GetRepository<WalletTransactionHistory>()
                    .Insert(walletHistoryForCustomer, walletHistoryForCustomerDeposit, walletHistoryForCarOwner, walletHistoryForCarOwnerDeposit);

                // Save changes to the database
                await _unitOfWork.SaveChanges();

                ViewBag.TotalAmountToPay = totalAmountToPay;
                TempData["success"] = "Deducted in user wallet successfully!";

                return false;
            }
            if (totalAmountToPay < totalDeposit)
            {
                var walletHistoryForCustomer = new WalletTransactionHistory()
                {
                    User = user,
                    UserId = user.Id,
                    TransactionDate = DateTime.Now,
                    Amount = (totalDeposit - totalAmountToPay), // Set the amount for customer's transaction
                    TransactionType = TransactionType.TopUp,
                    TransactionDescription = "Offset final payment in customer wallet!"
                };
                var walletHistoryForCustomerDeposit = new WalletTransactionHistory()
                {
                    User = user,
                    UserId = user.Id,
                    TransactionDate = DateTime.Now,
                    Amount = booking.Car.Deposit, // Set the amount for customer's deposit transaction
                    TransactionType = TransactionType.TopUp,
                    TransactionDescription = "Offset final payment in customer wallet!"
                };

                var walletHistoryForCarOwner = new WalletTransactionHistory()
                {
                    User = carOwner,
                    UserId = carOwner.Id,
                    TransactionDate = DateTime.Now,
                    Amount = (totalDeposit - totalAmountToPay), // Set the amount for car owner's transaction
                    TransactionType = TransactionType.WithDraw,
                    TransactionDescription = "Offset final payment in car owner wallet!"
                };
                var walletHistoryForCarOwnerDeposit = new WalletTransactionHistory()
                {
                    User = carOwner,
                    UserId = carOwner.Id,
                    TransactionDate = DateTime.Now,
                    Amount = booking.Car.Deposit, // Set the amount for car owner's deposit transaction
                    TransactionType = TransactionType.WithDraw,
                    TransactionDescription = "Offset final payment in car owner wallet!"
                };

                user.Wallet += (totalDeposit - totalAmountToPay);
                user.Wallet += booking.Car.Deposit;

                carOwner.Wallet -= (totalDeposit - totalAmountToPay);
                carOwner.Wallet -= booking.Car.Deposit;

                booking.Status = BookingStatus.Completed;
                booking.EndDateTime = DateTime.Now;

                _unitOfWork.GetRepository<Booking>().Update(booking);

                // Insert wallet transaction history records for customer and car owner
                await _unitOfWork.GetRepository<WalletTransactionHistory>()
                    .Insert(walletHistoryForCustomer, walletHistoryForCustomerDeposit, walletHistoryForCarOwner, walletHistoryForCarOwnerDeposit);

                // Save changes to the database
                await _unitOfWork.SaveChanges();

                ViewBag.TotalAmountToPay = totalAmountToPay;
                TempData["success"] = "Deducted in user wallet successfully!";

                return false;
            }
            else
            {
                var walletHistoryForCustomer = new WalletTransactionHistory()
                {
                    User = user,
                    UserId = user.Id,
                    TransactionDate = DateTime.Now,
                    Amount = booking.Car.Deposit, // Set the amount for customer's deposit transaction
                    TransactionType = TransactionType.TopUp,
                    TransactionDescription = "Offset final payment in customer wallet (Deposit)!"
                };

                var walletHistoryForCarOwner = new WalletTransactionHistory()
                {
                    User = carOwner,
                    UserId = carOwner.Id,
                    TransactionDate = DateTime.Now,
                    Amount = booking.Car.Deposit, // Set the amount for car owner's deposit transaction
                    TransactionType = TransactionType.WithDraw,
                    TransactionDescription = "Offset final payment in car owner wallet (Deposit)!"
                };

                user.Wallet += booking.Car.Deposit;
                carOwner.Wallet -= booking.Car.Deposit;

                booking.Status = BookingStatus.Completed;

                _unitOfWork.GetRepository<Booking>().Update(booking);

                // Insert wallet transaction history records for customer and car owner
                await _unitOfWork.GetRepository<WalletTransactionHistory>()
                    .Insert(walletHistoryForCustomer, walletHistoryForCarOwner);

                // Save changes to the database
                await _unitOfWork.SaveChanges();

                ViewBag.TotalAmountToPay = totalAmountToPay;
                TempData["success"] = "Deducted and deposited in user wallet successfully!";

                return true;
            }

        }
    }
}
