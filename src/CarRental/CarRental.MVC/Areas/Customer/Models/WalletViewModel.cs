﻿using CarRental.Core.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.MVC.Areas.Customer.Models
{
    public class WalletViewModel
    {
        public string UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public ApplicationUser User { get; set; }

        [Precision(18, 2)]
        public decimal Amount { get; set; }

        public DateTime TransactionDate { get; set; }

        public string TransactionType { get; set; }

        public string? TransactionDescription { get; set; }
        public decimal? Balance { get; set; }   
    }
}
