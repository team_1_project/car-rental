﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.MVC.Areas.Customer.Models
{
    public class BookingViewModel
    {
        public Guid? Id { get; set; } = Guid.NewGuid();
        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public string PaymentMethod { get; set; }

        public string Status { get; set; }

        //Renter information
        public string? Name { get; set; }

        public string? Email { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string? NationalIDNo { get; set; }

        public string? PhoneNo { get; set; }

        public string? WardCode { get; set; }

        public string? Ward { get; set; }

        public string? DistrictCode { get; set; }

        public string? District { get; set; }

        public string? CityCode { get; set; }

        public string? City { get; set; }
        public IFormFile? DrivingLicenseImage { get; set; }

        public string? DetailAddress { get; set; }


        //Driver's information

        public string? DriverName { get; set; }

        public string? DriverEmail { get; set; }

        public DateTime? DriverDateOfBirth { get; set; }

        public string? DriverNationalIDNo { get; set; }

        public string? DriverPhoneNo { get; set; }

        public string? DriverWardCode { get; set; }

        public string? DriverWard { get; set; }

        public string? DriverDistrictCode { get; set; }

        public string? DriverDistrict { get; set; }

        public string? DriverCityCode { get; set; }

        public string? DriverCity { get; set; }

        public IFormFile? DriverDrivingLicenseImage { get; set; }

        public string? DriverDetailAddress { get; set; }

        public int BookingCarCount = 0;
        public string? BookingNo { get; set; }

        [ForeignKey(nameof(Car))]
        public Guid? CarId { get; set; }

        public virtual Car? Car { get; set; }

        public virtual Feedback? Feedback { get; set; }

        public List<CarImage>? CarImages { get; set; }
    }
}
