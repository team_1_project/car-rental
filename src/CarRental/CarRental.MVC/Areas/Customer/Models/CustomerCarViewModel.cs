﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.MVC.Areas.Customer.Models
{
    public class CustomerCarViewModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string? Name { get; set; }

        public string? LicensePlate { get; set; }

        public int? BrandModelId { get; set; }

        public BrandModel? BrandModel { get; set; }

        public string? Color { get; set; }

        public int? NumberOfSeat { get; set; }

        public int? ProductionYear { get; set; }

        public string? TransmissionType { get; set; }

        public string? FuelType { get; set; }

        public decimal? Mileage { get; set; }

        public decimal? FuelConsumption { get; set; }

        public decimal? BasePrice { get; set; }

        public decimal? Deposit { get; set; }

        public int? AddressId { get; set; }

        public Address? Address { get; set; }

        public string? DetailAddress { get; set; }

        public string? Description { get; set; }

        [StringLength(255)]
        public string? AdditionalFunction { get; set; }

        [StringLength(255)]
        public string? TermsOfUser { get; set; }

        public string? Other { get; set; }

        public string? OtherSpecify { get; set; }

        public string? Images { get; set; }

        public virtual ApplicationUser? CarOwner { get; set; }

        public string? CarOwnerId { get; set; }

        public string? CarOwnerUsername { get; set; }

        public virtual List<Booking>? Bookings { get; set; }

        public virtual List<CarImage>? CarImages { get; set; } = new List<CarImage>();

        public IFormFile? ImageFileUploadRegistration { get; set; }

        public string? ImageUrlRegistration { get; set; }

        public IFormFile? ImageFileUploadCertificate { get; set; }

        public string? ImageUrlCertificate { get; set; }

        public IFormFile? ImageFileUploadInsurance { get; set; }

        public string? ImageUrlInsurance { get; set; }

        public IFormFile? ImageFileUploadFront { get; set; }

        public string? ImageUrlFront { get; set; }

        public IFormFile? ImageFileUploadBack { get; set; }

        public string? ImageUrlBack { get; set; }

        public IFormFile? ImageFileUploadLeft { get; set; }

        public string? ImageUrlLeft { get; set; }

        public IFormFile? ImageFileUploadRight { get; set; }

        public string? ImageUrlRight { get; set; }

        public int TotalRatings { get; set; } = 0;
        public int RatingCount { get; set; } = 0;
        public double AvgRate { get; set; } = 0;

        [NotMapped]
        public string FullText
        {
            get
            {
                if (Address != null)
                {
                    return $"{Address.Ward}, {Address.District}, {Address.City}";
                }
                return string.Empty;
            }
        }
    }
}
