﻿// File Upload
//
function ekUpload(id) {
    function Init() {
        console.log("Upload Initialised");

        var fileSelect = document.getElementById(`file-upload-${id}`),
            fileDrag = document.getElementById(`file-drag-${id}`),
            submitButton = document.getElementById(`submit-button-${id}`);

        fileSelect.addEventListener("change", fileSelectHandler, false);

        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // File Drop
            fileDrag.addEventListener("dragover", fileDragHover, false);
            fileDrag.addEventListener("dragleave", fileDragHover, false);
            fileDrag.addEventListener("drop", fileSelectHandler, false);
        }
    }

    function fileDragHover(e) {
        var fileDrag = document.getElementById(`file-drag-${id}`);

        e.stopPropagation();
        e.preventDefault();

        fileDrag.className =
            e.type === "dragover" ? "hover" : "modal-body file-upload";
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var files = e.target.files || e.dataTransfer.files;

        // Cancel event and hover styling
        fileDragHover(e);

        // Process all File objects
        for (var i = 0, f; (f = files[i]); i++) {
            parseFile(f);
            /*uploadFile(f);*/
        }
    }

    // Output
    function output(msg) {
        // Response
        var m = document.getElementById(`messages-${id}`);
        m.innerHTML = msg;
    }

    function parseFile(file) {
        console.log(file.name);
        output("<strong>" + encodeURI(file.name) + "</strong>");

        // var fileType = file.type;
        // console.log(fileType);
        var imageName = file.name;

        var isGood = /\.(?=gif|jpg|png|jpeg)/gi.test(imageName);
        if (isGood) {
            document.getElementById(`start-${id}`).classList.add("hidden");
            document.getElementById(`response-${id}`).classList.remove("hidden");
            document.getElementById(`notimage-${id}`).classList.add("hidden");
            // Thumbnail Preview
            document.getElementById(`file-image-${id}`).classList.remove("hidden");
            document.getElementById(`file-image-${id}`).src =
                URL.createObjectURL(file);
            let imageCarousel = document.getElementById(`carousel-image-${id}`);
            if (imageCarousel != null) {
                imageCarousel.src = URL.createObjectURL(file);
            }
        } else {
            document.getElementById(`file-image-${id}`).classList.add("hidden");
            document.getElementById(`notimage-${id}`).classList.remove("hidden");
            document.getElementById(`start-${id}`).classList.remove("hidden");
            document.getElementById(`response-${id}`).classList.add("hidden");
            document.getElementById(`file-upload-form-${id}`).reset();
        }
    }

    function setProgressMaxValue(e) {
        var pBar = document.getElementById(`file-progress-${id}`);

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById(`file-progress-${id}`);

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }

    function uploadFile(file) {
        var xhr = new XMLHttpRequest(),
            fileInput = document.getElementById(`class-roster-file-${id}`),
            pBar = document.getElementById(`file-progress-${id}`),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                pBar.style.display = "inline";
                xhr.upload.addEventListener("loadstart", setProgressMaxValue, false);
                xhr.upload.addEventListener("progress", updateFileProgress, false);

                // File received / failed
                xhr.onreadystatechange = function (e) {
                    if (xhr.readyState == 4) {
                        // Everything is good!
                        // progress.className = (xhr.status == 200 ? "success" : "failure");
                        // document.location.reload(true);
                    }
                };

                // Start upload
                xhr.open("POST", document.getElementById("signUpForm").action, true);
                xhr.setRequestHeader("X-File-Name", file.name);
                xhr.setRequestHeader("X-File-Size", file.size);
                xhr.setRequestHeader("Content-Type", "multipart/form-data");
                xhr.send(file);
            } else {
                output("Please upload a smaller file (< " + fileSizeLimit + " MB).");
            }
        }
    }

    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById(`file-drag-${id}`).style.display = "none";
    }
}

ekUpload("4");
ekUpload("5");
ekUpload("6");
ekUpload("7");
