﻿var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("step");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == x.length - 1) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n);
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("step");
    console.log(x);
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
        var userWallet = parseFloat("@ViewBag.Wallet");
        var totalDeposit = parseFloat("@ViewBag.TotalDeposit");

        if (userWallet < totalDeposit) {
            // Display an error message (you can use alert or another method)
            alert("Wallet balance is insufficient. Please recharge your wallet before submitting the form.");
            return false; // Prevent the form from submitting
        }
        document.getElementById("signUpForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    console.log("Check validation");
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("step");
    y = x[currentTab].getElementsByTagName("input"); // tab thu nhat : currentTab = 0
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        y[i].classList.remove("invalid");
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].classList.add("invalid");
            // and set the current valid status to false
            if (valid) {
                y[i].focus();
            }
            valid = false;
            console.log(y[i]);
        }

        if (y[i].id == "different" && !$("#different").prop("checked")) {
            valid = true;
            break;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("stepIndicator")[currentTab].className += " finish";
    }


    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i,
        x = document.getElementsByClassName("stepIndicator");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
}


// File Upload
//
function ekUpload(id) {
    function Init() {
        console.log("Upload Initialised");

        var fileSelect = document.getElementById(`file-upload-${id}`),
            fileDrag = document.getElementById(`file-drag-${id}`),
            submitButton = document.getElementById(`submit-button-${id}`);

        fileSelect.addEventListener("change", fileSelectHandler, false);

        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // File Drop
            fileDrag.addEventListener("dragover", fileDragHover, false);
            fileDrag.addEventListener("dragleave", fileDragHover, false);
            fileDrag.addEventListener("drop", fileSelectHandler, false);
        }
    }

    function fileDragHover(e) {
        var fileDrag = document.getElementById(`file-drag-${id}`);

        e.stopPropagation();
        e.preventDefault();

        fileDrag.className =
            e.type === "dragover" ? "hover" : "modal-body file-upload";
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var files = e.target.files || e.dataTransfer.files;

        // Cancel event and hover styling
        fileDragHover(e);

        // Process all File objects
        for (var i = 0, f; (f = files[i]); i++) {
            parseFile(f);
            uploadFile(f);
        }
    }

    // Output
    function output(msg) {
        // Response
        var m = document.getElementById(`messages-${id}`);
        m.innerHTML = msg;
    }

    function parseFile(file) {
        console.log(file.name);
        output("<strong>" + encodeURI(file.name) + "</strong>");

        // var fileType = file.type;
        // console.log(fileType);
        var imageName = file.name;

        var isGood = /\.(?=gif|jpg|png|jpeg)/gi.test(imageName);
        if (isGood) {
            document.getElementById(`start-${id}`).classList.add("hidden");
            document.getElementById(`response-${id}`).classList.remove("hidden");
            document.getElementById(`notimage-${id}`).classList.add("hidden");
            // Thumbnail Preview
            document.getElementById(`file-image-${id}`).classList.remove("hidden");
            document.getElementById(`file-image-${id}`).src = URL.createObjectURL(file);
        } else {
            document.getElementById(`file-image-${id}`).classList.add("hidden");
            document.getElementById(`notimage-${id}`).classList.remove("hidden");
            document.getElementById(`start-${id}`).classList.remove("hidden");
            document.getElementById(`response-${id}`).classList.add("hidden");
            document.getElementById(`file-upload-form-${id}`).reset();
        }
    }

    function setProgressMaxValue(e) {
        var pBar = document.getElementById(`file-progress-${id}`);

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById(`file-progress-${id}`);

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }

    function uploadFile(file) {
        var xhr = new XMLHttpRequest(),
            fileInput = document.getElementById(`class-roster-file-${id}`),
            pBar = document.getElementById(`file-progress-${id}`),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                pBar.style.display = "inline";
                xhr.upload.addEventListener("loadstart", setProgressMaxValue, false);
                xhr.upload.addEventListener("progress", updateFileProgress, false);

                // File received / failed
                xhr.onreadystatechange = function (e) {
                    if (xhr.readyState == 4) {
                        // Everything is good!
                        // progress.className = (xhr.status == 200 ? "success" : "failure");
                        // document.location.reload(true);
                    }
                };

                // Start upload
                xhr.open(
                    "POST",
                    document.getElementById("signUpForm").action,
                    true
                );
                xhr.setRequestHeader("X-File-Name", file.name);
                xhr.setRequestHeader("X-File-Size", file.size);
                xhr.setRequestHeader("Content-Type", "multipart/form-data");
                xhr.send(file);
            } else {
                output("Please upload a smaller file (< " + fileSizeLimit + " MB).");
            }
        }
    }

    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById(`file-drag-${id}`).style.display = "none";
    }
}

ekUpload("1");
ekUpload("2");