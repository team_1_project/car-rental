﻿using CarRental.Core.Models;
using CarRental.DataAccess.Infrastructure;
using CarRental.MVC.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace CarRental.MVC.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private readonly IUnitOfWork _unitOfWork;

		public HomeController(ILogger<HomeController> logger, IUnitOfWork unitOfWork)
		{
			_logger = logger;
			_unitOfWork = unitOfWork;
		}
		[AllowAnonymous]
		public async Task<IActionResult> Index()
        {
            var topCitiesBooking = await _unitOfWork.GetRepository<Address>()
                .GetQuery()
                .Include(x => x.Cars)
                .ThenInclude(x => x.Bookings)
                .SelectMany(address => address.Cars.SelectMany(car => car.Bookings),
                    (address, booking) => new { address.City, BookingCount = 1 })
                .GroupBy(x => x.City)
                .Select(group => new
                {
                    City = group.Key,
                    TotalBookings = group.Sum(x => x.BookingCount)
                })
                .OrderByDescending(x => x.TotalBookings)
                .Take(6)
                .ToListAsync();

            // Check if top cities list is less than 6, if less than 6, filter randomly rest of cities
            if (topCitiesBooking.Count < 6)
            {
                var additionalCities = await _unitOfWork.GetRepository<Address>()
                    .GetQuery()
                    .Where(x => !topCitiesBooking.Select(c => c.City).Contains(x.City))
                    .Select(x => new
                    {
                        City = x.City,
                        TotalBookings = 0
                    })
                    .ToListAsync();

                topCitiesBooking.AddRange(additionalCities);
            }

            // Ensure to have exactly 6 results
            topCitiesBooking = topCitiesBooking.Take(6).ToList();

            ViewBag.TopCitiesBooking = topCitiesBooking;

            // Feedback
            var feedback = await _unitOfWork.GetRepository<Feedback>()
                .GetQuery()
                .Include(x => x.Booking)
                .Take(6)
                .ToListAsync();
            ViewBag.Feedback = feedback;

            return View();
        }

		public IActionResult AboutUs()
		{
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}