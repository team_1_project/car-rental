﻿using CarRental.Core.Models;
using CarRental.DataAccess.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.MVC.Controllers
{
    public class CommonController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CommonController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet]
        public IActionResult GetBrands()
        {
            var brands = _unitOfWork.GetRepository<BrandModel>().GetQuery()
                .Select(b => b.Brand)
                .Distinct()
                .ToList();
            return Json(brands);
        }

        [HttpGet]
        public IActionResult GetModels(string brand)
        {
            var models = _unitOfWork.GetRepository<BrandModel>().GetQuery()
                .Where(b => b.Brand == brand)
                .Select(b => b.Model)
                .ToList();

            return Json(models);
        }


        [HttpGet]
        public IActionResult GetCities()
        {
            var cities = _unitOfWork.GetRepository<Address>().GetQuery()
                .OrderBy(a => a.City)
                .Select(a => a.City)
                .Distinct()
                .ToList();
            return Json(cities);
        }

        [HttpGet]
        public IActionResult GetDistricts(string city)
        {
            var districts = _unitOfWork.GetRepository<Address>().GetQuery()
                .Where(a => a.City == city)
                .OrderBy(a => a.District)
                .Select(a => a.District)
                .Distinct()
                .ToList();
            return Json(districts);
        }

        [HttpGet]
        public IActionResult GetWards(string city, string district)
        {
            var wards = _unitOfWork.GetRepository<Address>().GetQuery()
                .Where(a => a.City == city && a.District == district)
                .OrderBy(a => a.Ward)
                .Select(a => a.Ward)
                .Distinct()
                .ToList();

            return Json(wards);
        }
    }
}
