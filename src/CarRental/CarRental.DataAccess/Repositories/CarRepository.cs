using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.Core.ViewModels;
using CarRental.DataAccess.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace CarRental.DataAccess.Repositories
{
    public class CarRepository : GenericRepository<Car>, ICarRepository
    {
        public CarRepository(ApplicationDbContext context) : base(context)
        {
        }
        
        /// <summary>
        /// Gets the average star rating for a user from all feedbacks.
        /// </summary>
        /// <param name="user">The application user.</param>
        /// <returns>
        /// The average star rating or 0 if no feedbacks found.
        /// </returns>
        public double GetAverageStarForUser(ApplicationUser user)
        {
            var averageStar = context.Feedbacks
                .Where(x => x.Booking.Car.CarOwner.Id == user.Id)
                .Select(x => x.Ratings)
                .Average();

            return averageStar;
        }

        /// <summary>
        /// Gets a list of feedbacks filtered by star rating for a user.
        /// </summary>
        /// <param name="filter">The star rating filter.</param>
        /// <param name="user">The application user.</param>
        /// <returns>List of feedbacks matching filter.</returns>
        public async Task<List<Feedback>> GetFeedbacksByStar(string filter, ApplicationUser user)
        {
            var feedbacks = context.Feedbacks
                .Where(x => x.Booking.Car.CarOwner.Id == user.Id)
                .Include(x => x.Booking)
                .ThenInclude(x => x.Customer)
                .Include(x => x.Booking)
                .ThenInclude(x => x.Car)
                .ThenInclude(x => x.CarImages)
                .AsQueryable();

            if (filter != "All")
            {
                switch (filter)
                {
                    case "1": feedbacks = feedbacks.Where(x => x.Ratings == 1); break;
                    case "2": feedbacks = feedbacks.Where(x => x.Ratings == 2); break;
                    case "3": feedbacks = feedbacks.Where(x => x.Ratings == 3); break;
                    case "4": feedbacks = feedbacks.Where(x => x.Ratings == 4); break;
                    case "5": feedbacks = feedbacks.Where(x => x.Ratings == 5); break;
                }
            }

            return await feedbacks.ToListAsync();
        }
        
        /// <summary>
        /// Creates a new car entity in the database.
        /// </summary>
        /// <param name="carViewModel">Car view model containing new car details.</param>
        /// <param name="currentUser">Current logged in application user.</param>
        /// <param name="_mapper">Object mapper to map view model to entity.</param>
        /// <returns>
        /// The newly created <see cref="Car"/> entity if successful, otherwise null.
        /// </returns>
        public async Task<Car?> CreateCar(CarViewModel carViewModel, ApplicationUser? currentUser, IMapper _mapper)
        {
            Car car = new Car();
            car = _mapper.Map<Car>(carViewModel);

            var address = await context.Addresses
               .FirstOrDefaultAsync(x => carViewModel.Address != null
               && x.Ward == carViewModel.Address.Ward
               && x.District == carViewModel.Address.District
               && x.City == carViewModel.Address.City);

            var brandModel = await context.BrandModels
                .FirstOrDefaultAsync(x => carViewModel.BrandModel != null
                && x.Brand == carViewModel.BrandModel.Brand
                && x.Model == carViewModel.BrandModel.Model);

            if (address == null || brandModel == null)
            {
                return null;
            }

            car.AdditionalFunction = String.Join(",", carViewModel.AdditionalFunctions ?? new List<string>());
            car.TermsOfUser = String.Join(",", carViewModel.TermsOfUsers ?? new List<string>());

            if (carViewModel.Other != null || carViewModel.Other != "")
            {
                car.TermsOfUser = car.TermsOfUser + $",Other:{carViewModel.OtherSpecify}";
            }

            if (brandModel == null || address == null || currentUser == null)
            {
                return null;
            }

            car.Name = $"{brandModel.Brand} {brandModel.Model} {car.ProductionYear}";
            car.Address = address;
            car.AddressId = address.Id;
            car.BrandModel = brandModel;
            car.BrandModelId = brandModel.Id;
            car.CarOwner = currentUser;
            car.CarOwnerId = currentUser.Id;

            return car;
        }

        /// <summary>
        /// Edits the price details of an existing car entity.
        /// </summary>
        /// <param name="carViewModel">Car view model containing updated price details.</param>
        /// <returns>
        /// True if price details were successfully updated, otherwise false.
        /// </returns>
        public async Task<bool> EditPrice(EditCarViewModel carViewModel)
        {
            var car = await FirstOrDefaultInclude(filter: x => x.Id == carViewModel.Id,
                    includeProps: "Address,CarOwner,BrandModel,CarImages,Bookings");

            if (car == null)
            {
                return false;
            }

            car.BasePrice = carViewModel.BasePrice;
            car.Deposit = carViewModel.Deposit;
            car.TermsOfUser = String.Join(",", carViewModel.TermsOfUsers ?? new List<string>());

            if (carViewModel.Other != null || carViewModel.Other != "")
            {
                car.TermsOfUser = car.TermsOfUser + $",Other:{carViewModel.OtherSpecify}";
            }

            Update(car);
            await Save();

            return true;
        }

        /// <summary>
        /// Edits the details of an existing car entity.
        /// </summary>
        /// <param name="carViewModel">Car view model containing updated details.</param>
        /// <returns>
        /// The car entity with updated details if successful, otherwise null.
        /// </returns>
        public async Task<Car?> EditDetail(EditCarViewModel carViewModel)
        {
            var car = await
                FirstOrDefaultInclude(filter: x => x.Id == carViewModel.Id,
                    includeProps: "Address,CarOwner,BrandModel,CarImages,Bookings");

            var address = await context.Addresses
               .FirstOrDefaultAsync(x => carViewModel.Address != null
               && x.Ward == carViewModel.Address.Ward
               && x.District == carViewModel.Address.District
               && x.City == carViewModel.Address.City);

            if (car == null || address == null)
            {
                return null;
            }

            car.Mileage = carViewModel.Mileage;
            car.FuelConsumption = carViewModel.FuelConsumption;
            car.Address = address;
            car.AddressId = address.Id;
            car.Description = carViewModel.Description;
            car.AdditionalFunction = string.Join(",", carViewModel.AdditionalFunctions ?? new List<string>());

            return car;
        }
    }
}