using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarRental.Core.Models;
using CarRental.Core.ViewModels;
using CarRental.DataAccess.Infrastructure;

namespace CarRental.DataAccess.Repositories
{
    public interface ICarRepository : IGenericRepository<Car>
    {
        /// <summary>
        /// Creates a new car entity in the database.
        /// </summary>
        /// <param name="carViewModel">Car view model containing new car details.</param>
        /// <param name="currentUser">Current logged in application user.</param>
        /// <param name="_mapper">Object mapper to map view model to entity.</param>
        /// <returns>
        /// The newly created <see cref="Car"/> entity if successful, otherwise null.
        /// </returns>
        Task<Car?> CreateCar(CarViewModel carViewModel, ApplicationUser? currentUser, IMapper _mapper);
        
        /// <summary>
        /// Edits the details of an existing car entity.
        /// </summary>
        /// <param name="carViewModel">Car view model containing updated details.</param>
        /// <returns>
        /// The car entity with updated details if successful, otherwise null.
        /// </returns>
        Task<Car?> EditDetail(EditCarViewModel carViewModel);
        
        /// <summary>
        /// Edits the price details of an existing car entity.
        /// </summary>
        /// <param name="carViewModel">Car view model containing updated price details.</param>
        /// <returns>
        /// True if price details were successfully updated, otherwise false.
        /// </returns>
        Task<bool> EditPrice(EditCarViewModel carViewModel);
        
        /// <summary>
        /// Gets the average star rating for a user from all feedbacks.
        /// </summary>
        /// <param name="user">The application user.</param>
        /// <returns>
        /// The average star rating or 0 if no feedbacks found.
        /// </returns>
        double GetAverageStarForUser(ApplicationUser user);
        
        /// <summary>
        /// Gets a list of feedbacks filtered by star rating for a user.
        /// </summary>
        /// <param name="filter">The star rating filter.</param>
        /// <param name="user">The application user.</param>
        /// <returns>List of feedbacks matching filter.</returns>
        Task<List<Feedback>> GetFeedbacksByStar(string filter, ApplicationUser user);
    }
}