﻿using CarRental.Core.Common;
using CarRental.Core.Models;
using System.Linq.Expressions;

namespace CarRental.DataAccess.Infrastructure
{
    /// <summary>
    /// Represents a generic repository for database operations on entities of type T.
    /// </summary>
    /// <typeparam name="T">The type of entity that this repository works with.</typeparam>
    public interface IGenericRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Deletes an entity with the specified ID.
        /// </summary>
        /// <param name="id">The ID of the entity to be deleted.</param>
        Task Delete(object id);

        /// <summary>
        /// Deletes an entity from the repository.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        void Delete(T entity);

        /// <summary>
        /// Returns the first element of a sequence that satisfies a specified condition or a default value if no such element is found.
        /// </summary>
        /// <param name="filter">The filter expression to apply.</param>
        /// <returns>The first element that satisfies the condition, or a default value.</returns>
        Task<T?> FirstOrDefault(Expression<Func<T, bool>> filter);

        /// <summary>
        /// Returns the first element of a sequence that satisfies a specified condition or a default value if no such element is found.
        /// </summary>
        /// <param name="filter">The filter expression to apply.</param>
        /// <param name="includeProps">The properties to include in the query.</param>
        /// <returns>The first element that satisfies the condition, or a default value.</returns>
        Task<T?> FirstOrDefaultInclude(Expression<Func<T, bool>> filter, string? includeProps = null);

        /// <summary>
        /// Retrieves all entities from the repository.
        /// </summary>
        /// <param name="filter">The filter expression to apply.</param>
        /// <param name="orderBy">The ordering function to apply.</param>
        /// <param name="includeProps">The properties to include in the query.</param>
        /// <returns>A collection of entities.</returns>
        Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null, string? includeProps = null);

        /// <summary>
        /// Retrieves an entity by its ID.
        /// </summary>
        /// <param name="id">The ID of the entity to retrieve.</param>
        /// <returns>The retrieved entity, if found and active; otherwise, null.</returns>
        Task<T?> GetById(object id);

        /// <summary>
        /// Inserts one or more entities into the repository.
        /// </summary>
        /// <param name="entities">The entities to be inserted.</param>
        Task Insert(params T[] entities);

        /// <summary>
        /// Inserts a new entity into the repository.
        /// </summary>
        /// <param name="entity">The entity to be inserted.</param>
        Task Insert(T entity);

        /// <summary>
        /// Saves changes made in the context to the underlying database.
        /// </summary>
        Task Save();

        /// <summary>
        /// Updates an existing entity in the repository.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        void Update(T entity);

        /// <summary>
        /// Asynchronously gets paginated entities based on the provided criteria.
        /// </summary>
        /// <param name="filter">The filter expression.</param>
        /// <param name="orderBy">The order by expression.</param>
        /// <param name="includeProperties">The comma-separated list of navigation properties to include.</param>
        /// <param name="pageIndex">The index of the page to retrieve.</param>
        /// <param name="pageSize">The size of the page.</param>
        /// <returns>A task representing the asynchronous operation with the paginated result.</returns>
        Task<Paginated<T>> GetPaginatedAsync(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProps = "", int pageIndex = 1, int pageSize = 10);

        /// <summary>
        /// Gets a queryable collection of entities with optional filtering, sorting, and inclusion of deleted items.
        /// </summary>
        /// <param name="filter">A filter expression to apply to the query (optional).</param>
        /// <param name="orderBy">A function to specify sorting order for the query (optional).</param>
        /// <param name="isGetIncludeDeleted">A flag indicating whether to include deleted items in the query (optional).</param>
        /// <returns>A queryable collection of entities meeting the specified criteria.</returns>
        /// <remarks>
        /// This method allows you to retrieve entities from the repository with optional filtering and sorting.
        /// By default, it excludes deleted items unless 'isGetIncludeDeleted' is set to 'true'.
        /// </remarks>
        IQueryable<T> GetQuery(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            bool isGetIncludeDeleted = false);

        void HardDelete(object id);

        void HardDelete(T entity);

        void HardDelete(params T[] entity);
    }
}
