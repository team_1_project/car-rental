﻿using CarRental.Core.Models;
using CarRental.DataAccess.Repositories;

namespace CarRental.DataAccess.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Gets a repository for the specified entity type, creating a new one if it doesn't exist.
        /// </summary>
        /// <typeparam name="TEntity">The type of entity for which the repository is obtained.</typeparam>
        /// <returns>An instance of the repository for the specified entity type.</returns>
        /// <remarks>The repository is stored in a dictionary to ensure a single instance per entity type.</remarks>
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        /// Asynchronously saves changes made to the underlying data context.
        /// </summary>
        /// <returns>A task representing the asynchronous save operation.</returns>
        Task SaveChanges();

        ICarRepository GetCarRepository();
    }
}
