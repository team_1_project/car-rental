﻿using CarRental.Core.Common;
using CarRental.Core.Data;
using CarRental.Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace CarRental.DataAccess.Infrastructure
{
    /// <summary>
    /// Represents a generic repository for database operations on entities of type T.
    /// </summary>
    /// <typeparam name="T">The type of entity that this repository works with.</typeparam>
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// The database set containing entities of type T.
        /// </summary>
        protected internal readonly DbSet<T> dbSet;

        /// <summary>
        /// The database context associated with the repository.
        /// </summary>
        protected internal readonly ApplicationDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="context">The database context to be used by the repository.</param>
        public GenericRepository(ApplicationDbContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public virtual async Task Delete(object id)
        {
            T? entityToDelete = await dbSet.FindAsync(id);

            if (entityToDelete != null)
            {
                entityToDelete.IsActive = false;
                entityToDelete.UpdatedAt = DateTime.Now;
                context.Entry(entityToDelete).State = EntityState.Modified;
            }
        }

        public virtual async Task<T?> GetById(object id)
        {
            T? entity = await dbSet.FindAsync(id);

            if (entity != null && entity.IsActive == true)
            {
                return entity;
            }

            return null;
        }

        public virtual async Task Insert(T entity)
        {
            entity.UpdatedAt = DateTime.Now;
            entity.CreatedAt = DateTime.Now;
            await dbSet.AddAsync(entity);
        }

        public virtual void Update(T entity)
        {
            entity.UpdatedAt = DateTime.Now;
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }

            entity.IsActive = false;
            entity.UpdatedAt = DateTime.Now;

            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual async Task<IEnumerable<T>> GetAll(
            Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string? includeProps = null)
        {
            IQueryable<T> query = dbSet.Where(t => t.IsActive == true);

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProps != null)
            {
                foreach (var includeProperty in includeProps.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public virtual async Task Insert(params T[] entities)
        {
            for (int i = 0; i < entities.Length; i++)
            {
                await dbSet.AddAsync(entities[i]);
            }
        }

        public virtual async Task Save()
        {
            await context.SaveChangesAsync();
        }

        public virtual async Task<T?> FirstOrDefault(Expression<Func<T, bool>> filter)
        {
            return await dbSet.Where(t => t.IsActive == true).FirstOrDefaultAsync(filter);
        }

        public virtual async Task<T?> FirstOrDefaultInclude(Expression<Func<T, bool>> filter, string? includeProps = null)
        {
            var query = dbSet.AsQueryable();

            if (includeProps != null)
            {
                foreach (var includeProperty in includeProps.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            return await query.FirstOrDefaultAsync(filter);
        }

        public async Task<Paginated<T>> GetPaginatedAsync(Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null, string includeProps = "", int pageIndex = 1, int pageSize = 10)
        {
            IQueryable<T> query = dbSet.Where(t => t.IsActive == true);

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProps != null)
            {
                foreach (var includeProperty in includeProps.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return await Paginated<T>.CreateAsync(query.AsNoTracking(), pageIndex, pageSize);
        }

        public IQueryable<T> GetQuery(
            Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            bool isGetIncludeDeleted = false)
        {
            IQueryable<T> query = dbSet.AsQueryable();

            if (!isGetIncludeDeleted)
            {
                query = query.Where(t => t.IsActive == true);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query;
        }

        public void HardDelete(object id)
        {
            T? entityToDelete = dbSet.Find(id);

            if (entityToDelete != null)
            {
                dbSet.Remove(entityToDelete);
            }
        }

        public void HardDelete(T entity)
        {
            dbSet.Remove(entity);
        }

        public void HardDelete(params T[] entity)
        {
            dbSet.RemoveRange(entity);
        }
    }
}
