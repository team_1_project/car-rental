using CarRental.Core.Common;
using CarRental.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;

namespace CarRental.Core.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Booking> Bookings { get; set; }

        public DbSet<Car> Cars { get; set; }

        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<CarImage> CarImages { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<BrandModel> BrandModels { get; set; }

        public DbSet<WalletTransactionHistory> WalletTransactionHistories { get; set; }
    }
}
