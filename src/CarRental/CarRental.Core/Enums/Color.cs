namespace CarRental.Core.Enums
{
    public enum Color
    {
        White,
        Black,
        Gray,
        Silver,
        Red,
        Blue,
        Brown,
        Green,
        Beige,
        Gold,
        Yellow,
        Purple,

    }
}