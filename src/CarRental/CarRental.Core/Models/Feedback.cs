﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
    public class Feedback : BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public int Ratings { get; set; }
        public int? MaxRate { get; set; } = 5;

        [StringLength(255)]
        public string Content { get; set; }

        public DateTime DateTime { get; set; }

        public virtual Booking Booking { get; set; }

        [ForeignKey(nameof(Booking))]
        public Guid BookingId { get; set; }
    }
}
