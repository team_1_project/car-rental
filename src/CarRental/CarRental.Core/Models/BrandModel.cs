﻿namespace CarRental.Core.Models
{
    public class BrandModel : BaseEntity
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public virtual List<Car> Cars { get; set; } = new List<Car>();
    }
}
