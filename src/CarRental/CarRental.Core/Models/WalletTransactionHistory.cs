﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
    public class WalletTransactionHistory : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }

        [ForeignKey(nameof(UserId))]

        public ApplicationUser User { get; set; }

        public Guid? BookingId { get; set; }

        [ForeignKey(nameof(BookingId))]
        public Booking? Booking { get; set; }

        [Precision(18, 2)]
        public decimal Amount { get; set; }

        public DateTime TransactionDate { get; set; } = DateTime.Now;

        [StringLength(255)]
        public string TransactionType { get; set; }

        [StringLength(255)]
        public string? TransactionDescription { get; set; }
    }
}