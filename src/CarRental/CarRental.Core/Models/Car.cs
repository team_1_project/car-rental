using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
    public class Car : BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [StringLength(255)]
        public string? Name { get; set; }

        [StringLength(255)]
        public string LicensePlate { get; set; }

        [StringLength(255)]
        public int? BrandModelId { get; set; }

        public BrandModel? BrandModel { get; set; }

        [StringLength(255)]
        public string Color { get; set; }

        [StringLength(255)]
        public int NumberOfSeat { get; set; }

        [StringLength(255)]
        public int ProductionYear { get; set; }

        [StringLength(255)]
        public string TransmissionType { get; set; }

        [StringLength(255)]
        public string FuelType { get; set; }

        [Precision(18, 2)]
        public decimal Mileage { get; set; }

        [Precision(18, 2)]
        public decimal FuelConsumption { get; set; }

        [Precision(18, 2)]
        public decimal BasePrice { get; set; }

        [Precision(18, 2)]
        public decimal Deposit { get; set; }

        public Address? Address { get; set; }

        [ForeignKey(nameof(Address))]
        public int? AddressId { get; set; }

        [StringLength(255)]
        public string? DetailAddress { get; set; }

        [StringLength(255)]
        public string? Description { get; set; }

        [StringLength(255)]
        public string? AdditionalFunction { get; set; }

        [StringLength(255)]
        public string? TermsOfUser { get; set; }

        [StringLength(255)]
        public string? Images { get; set; }

        public bool IsStopRent { get; set; } = false;

        public virtual ApplicationUser CarOwner { get; set; }

        [ForeignKey(nameof(CarOwner))]
        public string CarOwnerId { get; set; }

        public virtual List<Booking> Bookings { get; set; } = new List<Booking>();

        public virtual List<CarImage> CarImages { get; set; } = new List<CarImage>();

        [NotMapped]
        public string FullText
        {
            get
            {
                if (Address != null)
                {
                    return $"{Address.Ward}, {Address.District}, {Address.City}";
                }
                return string.Empty;
            }
        }
    }
}
