﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
    public class Booking : BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [StringLength(255)]
        public string? Name { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(255)]
        public string? NationalIDNo { get; set; }

        [StringLength(255)]
        public string? PhoneNo { get; set; }

        [StringLength(255)]
        public string? DrivingLicense { get; set; }

        [StringLength(255)]
        public string? WardCode { get; set; }

        [StringLength(255)]
        public string? Ward { get; set; }

        [StringLength(255)]
        public string? DistrictCode { get; set; }

        [StringLength(255)]
        public string? District { get; set; }

        [StringLength(255)]
        public string? CityCode { get; set; }

        [StringLength(255)]
        public string? City { get; set; }

        [StringLength(255)]
        public string? DetailAddress { get; set; }

        [StringLength(50)]
        public string BookingNo { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        [StringLength(255)]
        public string PaymentMethod { get; set; }

        [StringLength(255)]
        public string Status { get; set; }

        [StringLength(255)]
        public string? DriverName { get; set; }

        public DateTime? DriverDateOfBirth { get; set; }

        [StringLength(255)]
        public string? DriverNationalIDNo { get; set; }

        [StringLength(255)]
        public string? DriverPhoneNo { get; set; }

        [StringLength(255)]
        public string? DriverDrivingLicense { get; set; }

        [StringLength(255)]
        public string? DriverWardCode { get; set; }

        [StringLength(255)]
        public string? DriverWard { get; set; }

        [StringLength(255)]
        public string? DriverDistrictCode { get; set; }

        [StringLength(255)]
        public string? DriverDistrict { get; set; }

        [StringLength(255)]
        public string? DriverCityCode { get; set; }

        [StringLength(255)]
        public string? DriverCity { get; set; }

        [StringLength(255)]
        public string? DriverDetailAddress { get; set; }

        [ForeignKey(nameof(Car))]
        public Guid? CarId { get; set; }

        public virtual Car? Car { get; set; }

        public virtual ApplicationUser? Customer { get; set; }

        [ForeignKey(nameof(Customer))]
        public string? CustomerId { get; set; }

        public virtual Feedback? Feedback { get; set; }
    }
}