﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Core.Models
{
    public class CarImage : BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [StringLength(255)]
        public string Url { get; set; }

        [StringLength(255)]
        public string? Description { get; set; }


        [ForeignKey(nameof(Car))]
        public Guid CarId { get; set; }

        public Car? Car { get; set; }
    }
}
