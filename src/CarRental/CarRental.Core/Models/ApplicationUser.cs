﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string? Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string? NationalIDNo { get; set; }

        public string? PhoneNo { get; set; }

        public Address? Address { get; set; }

        [ForeignKey(nameof(Address))]
        public int? AddressId { get; set; }

        public string? DetailAddress { get; set; }

        public string? DrivingLicense { get; set; }

        [Precision(18, 2)]
        public decimal Wallet { get; set; }

        public virtual List<Car> Cars { get; set; } = new List<Car>();
    }
}
