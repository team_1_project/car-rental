﻿namespace CarRental.Core.Models
{
    public class Address : BaseEntity
    {
        public int Id { get; set; }

        public string WardCode { get; set; }

        public string Ward { get; set; }

        public string DistrictCode { get; set; }

        public string District { get; set; }

        public string CityCode { get; set; }

        public string City { get; set; }

        public virtual List<ApplicationUser> Users { get; set; } = new List<ApplicationUser>();

        public virtual List<Car> Cars { get; set; } = new List<Car>();

    }
}
