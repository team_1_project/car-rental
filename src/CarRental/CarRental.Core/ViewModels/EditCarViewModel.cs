﻿using CarRental.Core.Models;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.ViewModels
{
    public class EditCarViewModel
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public string? LicensePlate { get; set; }

        public int? BrandModelId { get; set; }

        public BrandModel? BrandModel { get; set; }

        public string? Color { get; set; }

        public int? NumberOfSeat { get; set; }

        public int? ProductionYear { get; set; }

        public string? TransmissionType { get; set; }

        public string? FuelType { get; set; }

        [Required]
        public decimal Mileage { get; set; }

        [Required]
        public decimal FuelConsumption { get; set; }

        [Required]
        public decimal BasePrice { get; set; }

        [Required]
        public decimal Deposit { get; set; }

        public int? AddressId { get; set; }

        public Address? Address { get; set; }

        public string? DetailAddress { get; set; }

        public string? Description { get; set; }

        public List<string>? AdditionalFunctions { get; set; }

        public List<string>? TermsOfUsers { get; set; }

        public string? Other { get; set; }

        public string? OtherSpecify { get; set; }

        public string? Images { get; set; }

        public virtual ApplicationUser? CarOwner { get; set; }

        public string? CarOwnerId { get; set; }

        public string? CarOwnerUsername { get; set; }

        public virtual List<Booking>? Bookings { get; set; }

        public virtual List<CarImage>? CarImages { get; set; } = new List<CarImage>();

        public IFormFile? ImageFileUploadRegistration { get; set; }

        public string? ImageUrlRegistration { get; set; }

        public IFormFile? ImageFileUploadCertificate { get; set; }

        public string? ImageUrlCertificate { get; set; }

        public IFormFile? ImageFileUploadInsurance { get; set; }

        public string? ImageUrlInsurance { get; set; }

        public IFormFile? ImageFileUploadFront { get; set; }

        public string? ImageUrlFront { get; set; }

        public IFormFile? ImageFileUploadBack { get; set; }

        public string? ImageUrlBack { get; set; }

        public IFormFile? ImageFileUploadLeft { get; set; }

        public string? ImageUrlLeft { get; set; }

        public IFormFile? ImageFileUploadRight { get; set; }

        public string? ImageUrlRight { get; set; }

        public bool? IsStopRent { get; set; }

    }
}
