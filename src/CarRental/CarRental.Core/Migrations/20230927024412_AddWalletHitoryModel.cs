﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class AddWalletHitoryModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "62d9fbc0-409a-46ee-af31-f3dc989408df", "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "a4c61962-d81f-45a1-8365-6602098ccd9a", "7f1a09e0-ea33-4284-874f-3f9e0f5a4643" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "7b39de24-c120-467c-b5dc-2febd21a9ff5", "f67de744-795e-41d3-a208-65f1e4952681" });

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("34e343b8-f7ac-42f9-8ed3-4fe8985c8405"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("51fd03b9-67cb-433f-9f59-f522f9da021e"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("814447c9-d306-4519-a68b-3fe4692f601c"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("a7619a0f-e42c-4b12-87f7-43e66f79746f"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("b7881ad9-b1bf-4641-9b81-1d11435f12bf"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("1c7b4c5e-b538-4423-9c91-7be0b79b8a4c"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("22246157-606c-4837-838d-8344764a6c96"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("5dae25f3-9884-48b0-955d-c75c7ad24aba"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("c87be9bf-43dc-44ba-ba4c-9cff1d3333a9"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "62d9fbc0-409a-46ee-af31-f3dc989408df");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7b39de24-c120-467c-b5dc-2febd21a9ff5");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a4c61962-d81f-45a1-8365-6602098ccd9a");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "7f1a09e0-ea33-4284-874f-3f9e0f5a4643");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("e6329ad7-27bf-49c3-bf01-db385cbad599"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("fb571300-e935-44da-846c-e76c0e3a6f47"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f67de744-795e-41d3-a208-65f1e4952681");

            migrationBuilder.CreateTable(
                name: "WalletTransactionHistories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    TransactionDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TransactionType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TransactionDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WalletTransactionHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WalletTransactionHistories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "10ac869e-9306-420e-a584-a0ab2b522d51", "27/09/2023 9:44:05 AM", "Admin", "ADMIN" },
                    { "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7", "27/09/2023 9:44:05 AM", "Customer", "CUSTOMER" },
                    { "b6e2f941-bce9-4057-8ee3-14da97a9ec0f", "27/09/2023 9:44:05 AM", "CarOwner", "CAROWNER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "0754ad7e-7a89-498e-b054-016e03e7b6d1", 0, null, "8552dda0-5425-4b5e-a6cc-f4c23c1ed434", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEB5CKF5XOnF8THaT9J3Fv2B08mpdqO1KoQizOBgU6OVxWCksLt/m3gqMhAZttSU8wA==", null, null, false, "41c30945-0fd7-4b1b-990e-b6171e2d8d31", false, "admin@email.com", 0m },
                    { "2e3ab1e8-7b4f-4343-92e1-98936af87e38", 0, null, "6a4d12f1-9df1-43dd-846b-e4e4febf9d6f", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEGGarWQ6uWOdgSGyj7pZAGaX1aEcxGBi4cXB0St5IEDr5/2wqrxs0W/uMrDvLI9POw==", null, null, false, "4a00130e-1b7b-47a2-b5d9-44a1192546a1", false, "carOwner@email.com", 0m },
                    { "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", 0, null, "98a92998-eba7-490f-83aa-952acb8b569a", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAECfvNHLolC7UaY9qtrfE1vM8ioV8HeYMtnEyzSAwp1aze1ZkJwI3SJzWSt7KIhMU7w==", null, null, false, "287d1243-734a-4f11-988b-a1c6bd91ce4e", false, "customer@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "10ac869e-9306-420e-a584-a0ab2b522d51", "0754ad7e-7a89-498e-b054-016e03e7b6d1" },
                    { "b6e2f941-bce9-4057-8ee3-14da97a9ec0f", "2e3ab1e8-7b4f-4343-92e1-98936af87e38" },
                    { "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7", "5371c83d-e0bb-48eb-9bb2-da49162d0fe9" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), "GPS, Bluetooth", 2, 50.0m, 2, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Blue", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7672), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7687) },
                    { new Guid("851fa2bf-afe1-4254-b8c8-8c9dfa41932d"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Silver", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7733), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7734) },
                    { new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"), "USB, Sunroof", 3, 45.0m, 3, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Red", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7718), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7719) },
                    { new Guid("9e5333e0-1b60-4904-98f9-1e95aff995bb"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "White", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7766), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7767) },
                    { new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"), "DVD, Leather Seats", 4, 60.0m, 4, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Black", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7727), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7727) }
                });

            migrationBuilder.InsertData(
                table: "Bookings",
                columns: new[] { "Id", "BookingNo", "CarId", "CreatedAt", "CustomerId", "DriverCity", "DriverCityCode", "DriverDateOfBirth", "DriverDetailAddress", "DriverDistrict", "DriverDistrictCode", "DriverDrivingLicense", "DriverName", "DriverNationalIDNo", "DriverPhoneNo", "DriverWard", "DriverWardCode", "EndDateTime", "IsActive", "PaymentMethod", "StartDateTime", "Status", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("16cd8019-94a8-4b44-b850-529a9e50d9dc"), "Booking002", new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7827), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City Z", "C456", new DateTime(1990, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "456 Elm Street", "District Y", "D456", "DL987654", "Jane Smith", "9876543210", "987-654-3210", "Ward B", "W456", new DateTime(2023, 9, 27, 7, 44, 5, 831, DateTimeKind.Utc).AddTicks(7831), true, "PayPal", new DateTime(2023, 9, 27, 5, 44, 5, 831, DateTimeKind.Utc).AddTicks(7830), "Pending", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7828) },
                    { new Guid("575fc48e-f5df-4594-bf4c-78c526df73d0"), "Booking003", new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7836), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City W", "C789", new DateTime(1985, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "789 Oak Avenue", "District Z", "D789", "DL555555", "Alice Johnson", "1357924680", "555-123-4567", "Ward C", "W789", new DateTime(2023, 9, 27, 10, 44, 5, 831, DateTimeKind.Utc).AddTicks(7841), true, "Cash", new DateTime(2023, 9, 27, 8, 44, 5, 831, DateTimeKind.Utc).AddTicks(7840), "Confirmed", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7836) },
                    { new Guid("92274bf6-66d5-415b-a180-21225f8a60ed"), "Booking001", new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7800), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City Y", "C123", new DateTime(1980, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "123 Main Street", "District X", "D123", "DL123456", "John Doe", "1234567890", "123-456-7890", "Ward A", "W123", new DateTime(2023, 9, 27, 4, 44, 5, 831, DateTimeKind.Utc).AddTicks(7804), true, "Credit Card", new DateTime(2023, 9, 27, 2, 44, 5, 831, DateTimeKind.Utc).AddTicks(7803), "Confirmed", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7801) },
                    { new Guid("ec968489-5d89-408d-bdda-efffe84ca736"), "Booking005", new Guid("9e5333e0-1b60-4904-98f9-1e95aff995bb"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7851), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City C", "C222", new DateTime(1988, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "222 Maple Street", "District B", "D222", "DL888888", "Eve Davis", "3698521470", "888-555-1212", "Ward E", "W222", new DateTime(2023, 9, 27, 16, 44, 5, 831, DateTimeKind.Utc).AddTicks(7853), true, "PayPal", new DateTime(2023, 9, 27, 14, 44, 5, 831, DateTimeKind.Utc).AddTicks(7853), "Pending", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7851) },
                    { new Guid("f55d9385-f1bf-444f-a9ef-364d086ccf84"), "Booking004", new Guid("851fa2bf-afe1-4254-b8c8-8c9dfa41932d"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7844), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City B", "C101", new DateTime(1992, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "101 Pine Street", "District A", "D101", "DL777777", "Bob Smith", "2468135790", "777-987-6543", "Ward D", "W101", new DateTime(2023, 9, 27, 13, 44, 5, 831, DateTimeKind.Utc).AddTicks(7847), true, "Credit Card", new DateTime(2023, 9, 27, 11, 44, 5, 831, DateTimeKind.Utc).AddTicks(7846), "Confirmed", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7845) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("309f44fa-a3f5-4fe9-8d42-c95ee741da15"), new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7796), "Front view of Car2", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7797), "car2_image1.jpg" },
                    { new Guid("5bd0a58c-e9b3-4ee0-a16d-dfc0c1079597"), new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7777), "Front view of Car1", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7778), "car1_image1.jpg" },
                    { new Guid("9825bfdd-b12d-4a96-ae3b-61b243a29926"), new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7790), "Interior of Car1", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7791), "car1_image2.jpg" },
                    { new Guid("afab0ac4-8563-4bf5-8bf5-ec724f7f94ad"), new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7787), "Back view of Car1", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7787), "car1_image2.jpg" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_WalletTransactionHistories_UserId",
                table: "WalletTransactionHistories",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WalletTransactionHistories");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "10ac869e-9306-420e-a584-a0ab2b522d51", "0754ad7e-7a89-498e-b054-016e03e7b6d1" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "b6e2f941-bce9-4057-8ee3-14da97a9ec0f", "2e3ab1e8-7b4f-4343-92e1-98936af87e38" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7", "5371c83d-e0bb-48eb-9bb2-da49162d0fe9" });

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("16cd8019-94a8-4b44-b850-529a9e50d9dc"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("575fc48e-f5df-4594-bf4c-78c526df73d0"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("92274bf6-66d5-415b-a180-21225f8a60ed"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("ec968489-5d89-408d-bdda-efffe84ca736"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("f55d9385-f1bf-444f-a9ef-364d086ccf84"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("309f44fa-a3f5-4fe9-8d42-c95ee741da15"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("5bd0a58c-e9b3-4ee0-a16d-dfc0c1079597"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("9825bfdd-b12d-4a96-ae3b-61b243a29926"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("afab0ac4-8563-4bf5-8bf5-ec724f7f94ad"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "10ac869e-9306-420e-a584-a0ab2b522d51");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b6e2f941-bce9-4057-8ee3-14da97a9ec0f");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0754ad7e-7a89-498e-b054-016e03e7b6d1");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "5371c83d-e0bb-48eb-9bb2-da49162d0fe9");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("851fa2bf-afe1-4254-b8c8-8c9dfa41932d"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9e5333e0-1b60-4904-98f9-1e95aff995bb"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2e3ab1e8-7b4f-4343-92e1-98936af87e38");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "62d9fbc0-409a-46ee-af31-f3dc989408df", "25/09/2023 10:49:25 AM", "Admin", "ADMIN" },
                    { "7b39de24-c120-467c-b5dc-2febd21a9ff5", "25/09/2023 10:49:25 AM", "CarOwner", "CAROWNER" },
                    { "a4c61962-d81f-45a1-8365-6602098ccd9a", "25/09/2023 10:49:25 AM", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3", 0, null, "14910ddf-2ed4-4984-b3cf-a4d4c90ed1e2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEOZA+0lf0HP3csi5Nz2Zx8CmgAjpDpYbrkUV2MKOkNjJ/fMN/tmQ9bhGzOpsjij3Yw==", null, null, false, "5bf283d4-b7ce-467d-a400-b7ad52079ee3", false, "admin@email.com", 0m },
                    { "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", 0, null, "08fced0e-f918-4237-937b-7dd04fe3671d", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEG2FzG6SzmzujLmgp0bs8FIx1epkw8yvxSzn6mVqQdZudgme7ZGYcAlde7uC/S2MgA==", null, null, false, "a6898585-12c2-4ee0-ac42-85ac013826c3", false, "customer@email.com", 0m },
                    { "f67de744-795e-41d3-a208-65f1e4952681", 0, null, "b8c3a820-f2d4-4220-afc6-b225d895b284", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEAOdyvyAsTbYGntbrpxmrdNAT3TVUIORBw4jz10ekS9DdcXLMsm6dYqC0iHvMJ3XwQ==", null, null, false, "205847c5-6c48-44ec-8d03-42e9df3869a0", false, "carOwner@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "62d9fbc0-409a-46ee-af31-f3dc989408df", "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3" },
                    { "a4c61962-d81f-45a1-8365-6602098ccd9a", "7f1a09e0-ea33-4284-874f-3f9e0f5a4643" },
                    { "7b39de24-c120-467c-b5dc-2febd21a9ff5", "f67de744-795e-41d3-a208-65f1e4952681" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), "GPS, Bluetooth", 2, 50.0m, 2, "f67de744-795e-41d3-a208-65f1e4952681", "Blue", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5150), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5156) },
                    { new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"), "USB, Sunroof", 3, 45.0m, 3, "f67de744-795e-41d3-a208-65f1e4952681", "Red", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5176), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5176) },
                    { new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"), "DVD, Leather Seats", 4, 60.0m, 4, "f67de744-795e-41d3-a208-65f1e4952681", "Black", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5183), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5183) },
                    { new Guid("e6329ad7-27bf-49c3-bf01-db385cbad599"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "f67de744-795e-41d3-a208-65f1e4952681", "Silver", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5193), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5193) },
                    { new Guid("fb571300-e935-44da-846c-e76c0e3a6f47"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "f67de744-795e-41d3-a208-65f1e4952681", "White", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5201), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5202) }
                });

            migrationBuilder.InsertData(
                table: "Bookings",
                columns: new[] { "Id", "BookingNo", "CarId", "CreatedAt", "CustomerId", "DriverCity", "DriverCityCode", "DriverDateOfBirth", "DriverDetailAddress", "DriverDistrict", "DriverDistrictCode", "DriverDrivingLicense", "DriverName", "DriverNationalIDNo", "DriverPhoneNo", "DriverWard", "DriverWardCode", "EndDateTime", "IsActive", "PaymentMethod", "StartDateTime", "Status", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("34e343b8-f7ac-42f9-8ed3-4fe8985c8405"), "Booking004", new Guid("e6329ad7-27bf-49c3-bf01-db385cbad599"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5267), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City B", "C101", new DateTime(1992, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "101 Pine Street", "District A", "D101", "DL777777", "Bob Smith", "2468135790", "777-987-6543", "Ward D", "W101", new DateTime(2023, 9, 25, 14, 49, 25, 131, DateTimeKind.Utc).AddTicks(5269), true, "Credit Card", new DateTime(2023, 9, 25, 12, 49, 25, 131, DateTimeKind.Utc).AddTicks(5268), "Confirmed", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5267) },
                    { new Guid("51fd03b9-67cb-433f-9f59-f522f9da021e"), "Booking005", new Guid("fb571300-e935-44da-846c-e76c0e3a6f47"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5272), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City C", "C222", new DateTime(1988, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "222 Maple Street", "District B", "D222", "DL888888", "Eve Davis", "3698521470", "888-555-1212", "Ward E", "W222", new DateTime(2023, 9, 25, 17, 49, 25, 131, DateTimeKind.Utc).AddTicks(5274), true, "PayPal", new DateTime(2023, 9, 25, 15, 49, 25, 131, DateTimeKind.Utc).AddTicks(5274), "Pending", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5272) },
                    { new Guid("814447c9-d306-4519-a68b-3fe4692f601c"), "Booking002", new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5252), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City Z", "C456", new DateTime(1990, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "456 Elm Street", "District Y", "D456", "DL987654", "Jane Smith", "9876543210", "987-654-3210", "Ward B", "W456", new DateTime(2023, 9, 25, 8, 49, 25, 131, DateTimeKind.Utc).AddTicks(5256), true, "PayPal", new DateTime(2023, 9, 25, 6, 49, 25, 131, DateTimeKind.Utc).AddTicks(5255), "Pending", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5252) },
                    { new Guid("a7619a0f-e42c-4b12-87f7-43e66f79746f"), "Booking003", new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5261), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City W", "C789", new DateTime(1985, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "789 Oak Avenue", "District Z", "D789", "DL555555", "Alice Johnson", "1357924680", "555-123-4567", "Ward C", "W789", new DateTime(2023, 9, 25, 11, 49, 25, 131, DateTimeKind.Utc).AddTicks(5263), true, "Cash", new DateTime(2023, 9, 25, 9, 49, 25, 131, DateTimeKind.Utc).AddTicks(5263), "Confirmed", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5262) },
                    { new Guid("b7881ad9-b1bf-4641-9b81-1d11435f12bf"), "Booking001", new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5230), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City Y", "C123", new DateTime(1980, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "123 Main Street", "District X", "D123", "DL123456", "John Doe", "1234567890", "123-456-7890", "Ward A", "W123", new DateTime(2023, 9, 25, 5, 49, 25, 131, DateTimeKind.Utc).AddTicks(5233), true, "Credit Card", new DateTime(2023, 9, 25, 3, 49, 25, 131, DateTimeKind.Utc).AddTicks(5232), "Confirmed", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5231) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("1c7b4c5e-b538-4423-9c91-7be0b79b8a4c"), new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5225), "Interior of Car1", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5225), "car1_image2.jpg" },
                    { new Guid("22246157-606c-4837-838d-8344764a6c96"), new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5210), "Front view of Car1", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5211), "car1_image1.jpg" },
                    { new Guid("5dae25f3-9884-48b0-955d-c75c7ad24aba"), new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5219), "Back view of Car1", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5220), "car1_image2.jpg" },
                    { new Guid("c87be9bf-43dc-44ba-ba4c-9cff1d3333a9"), new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5227), "Front view of Car2", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5228), "car2_image1.jpg" }
                });
        }
    }
}
