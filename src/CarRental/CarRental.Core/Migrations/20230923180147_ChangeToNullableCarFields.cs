﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class ChangeToNullableCarFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "097d30ac-85e3-415a-b14b-256e9bc2cbe6", "8a3fe731-a014-4ac0-940f-cb461d84cf2f" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "25b977b1-60c6-46c2-9433-7bd17c819757", "8ef93d3f-c02b-4c50-a740-a4d1d0832296" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "b8d3b6a4-2e37-4a71-9e20-ddab2277ba7c", "91205127-10e4-45b0-be2e-f3c0b94fa16e" });

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("46b5cc2c-e374-4244-bc02-33fc413354df"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("a3e1e7fb-9cd2-41cc-9a92-33615c3fbaef"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("c9ba9473-3fee-4462-a855-c450a7b096e4"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("cf0c67e7-717b-425c-9baf-ef1db2c5328a"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("49057f0e-a5f3-4e87-8632-b83f2f180dc5"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("be8778d9-4fb5-4f72-a519-2dad1c7ce67b"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "097d30ac-85e3-415a-b14b-256e9bc2cbe6");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "25b977b1-60c6-46c2-9433-7bd17c819757");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b8d3b6a4-2e37-4a71-9e20-ddab2277ba7c");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8a3fe731-a014-4ac0-940f-cb461d84cf2f");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "91205127-10e4-45b0-be2e-f3c0b94fa16e");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("507d2b9b-b1d0-4845-8e10-454288d883f6"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("6a20603d-67a5-41e9-adfc-b6193ab9fb6e"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9be5dfc5-b55e-453c-a91f-c6377362f2c2"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8ef93d3f-c02b-4c50-a740-a4d1d0832296");

            migrationBuilder.AlterColumn<string>(
                name: "TermsOfUser",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Images",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "AdditionalFunction",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "0c39e186-f01b-4470-a428-7cb795fcc24d", "24/09/2023 1:01:42 AM", "CarOwner", "CAROWNER" },
                    { "4335fafa-ecd7-4ab0-8b50-7f5114d10882", "24/09/2023 1:01:42 AM", "Admin", "ADMIN" },
                    { "ab1fbc30-00da-4581-99ae-79ae5be36f3b", "24/09/2023 1:01:42 AM", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8072d963-65ed-468b-9e41-8cfd5ec830d7", 0, null, "0efe418f-b01f-4d79-9446-3910c980ce91", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEAzgDqWQSjGFzInLUkEGCt0iOFNnhXW4ALXvMvYpuM2jat7V2TgjK7NV/SsYWGBoBQ==", null, null, false, "64423939-94f5-4a7f-b7d9-0cc5030fa6d9", false, "admin@email.com", 0m },
                    { "d01587bd-621c-449b-b4e3-4036d46af9da", 0, null, "a7e7b9f4-f0ed-437a-9033-0fd6dd72c2bd", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAELo5ETvuk80GTPHD8HX4a7KhJt+SmkG5pS5CONL7Vnn7a4rhNM0T1YVmbTCYlA8TVg==", null, null, false, "3b122cf8-c809-4d0e-b59f-1a0db0456712", false, "customer@email.com", 0m },
                    { "e3828257-0bc0-4f7e-9290-59fd14822aba", 0, null, "d9d7901f-5394-4487-8b1a-da7c1849ae8a", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEB3/tZfJLAzX9aQ1JINh7VXUZcXHAjLIn33IfDSlhT7IrAu/7rLg+yWWEIdktbgcfw==", null, null, false, "840f749d-4172-4059-8b57-43946df5e255", false, "carOwner@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "4335fafa-ecd7-4ab0-8b50-7f5114d10882", "8072d963-65ed-468b-9e41-8cfd5ec830d7" },
                    { "ab1fbc30-00da-4581-99ae-79ae5be36f3b", "d01587bd-621c-449b-b4e3-4036d46af9da" },
                    { "0c39e186-f01b-4470-a428-7cb795fcc24d", "e3828257-0bc0-4f7e-9290-59fd14822aba" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"), "GPS, Bluetooth", 2, 50.0m, 2, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Blue", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(805), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(810) },
                    { new Guid("5058a0d2-8aa3-45a8-9fd6-c6df06d5b36e"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "e3828257-0bc0-4f7e-9290-59fd14822aba", "White", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(848), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(848) },
                    { new Guid("64d725a1-a9ad-487f-8568-8e844ce08871"), "USB, Sunroof", 3, 45.0m, 3, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Red", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(825), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(826) },
                    { new Guid("acd51528-4157-4afe-b061-bdb9729b9df2"), "DVD, Leather Seats", 4, 60.0m, 4, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Black", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(836), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(837) },
                    { new Guid("f4273f29-6d5e-4d9a-85db-e42bc3a7742a"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Silver", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(842), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(842) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("0c8ed75d-7816-463a-aef6-5bb9de42d7c9"), new Guid("acd51528-4157-4afe-b061-bdb9729b9df2"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(868), "Front view of Car2", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(869), "car2_image1.jpg" },
                    { new Guid("11ca7f01-e617-4462-a31e-e834ceee4399"), new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(863), "Back view of Car1", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(863), "car1_image2.jpg" },
                    { new Guid("6e050c07-7cd3-4fb5-a2cc-89e99462253d"), new Guid("64d725a1-a9ad-487f-8568-8e844ce08871"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(866), "Interior of Car1", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(866), "car1_image2.jpg" },
                    { new Guid("89d06c5d-8fbd-4a0e-a580-a725e71245d3"), new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(856), "Front view of Car1", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(856), "car1_image1.jpg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4335fafa-ecd7-4ab0-8b50-7f5114d10882", "8072d963-65ed-468b-9e41-8cfd5ec830d7" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "ab1fbc30-00da-4581-99ae-79ae5be36f3b", "d01587bd-621c-449b-b4e3-4036d46af9da" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "0c39e186-f01b-4470-a428-7cb795fcc24d", "e3828257-0bc0-4f7e-9290-59fd14822aba" });

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("0c8ed75d-7816-463a-aef6-5bb9de42d7c9"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("11ca7f01-e617-4462-a31e-e834ceee4399"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("6e050c07-7cd3-4fb5-a2cc-89e99462253d"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("89d06c5d-8fbd-4a0e-a580-a725e71245d3"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("5058a0d2-8aa3-45a8-9fd6-c6df06d5b36e"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("f4273f29-6d5e-4d9a-85db-e42bc3a7742a"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0c39e186-f01b-4470-a428-7cb795fcc24d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4335fafa-ecd7-4ab0-8b50-7f5114d10882");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ab1fbc30-00da-4581-99ae-79ae5be36f3b");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8072d963-65ed-468b-9e41-8cfd5ec830d7");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "d01587bd-621c-449b-b4e3-4036d46af9da");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("64d725a1-a9ad-487f-8568-8e844ce08871"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("acd51528-4157-4afe-b061-bdb9729b9df2"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "e3828257-0bc0-4f7e-9290-59fd14822aba");

            migrationBuilder.AlterColumn<string>(
                name: "TermsOfUser",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Images",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AdditionalFunction",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "097d30ac-85e3-415a-b14b-256e9bc2cbe6", "22/09/2023 2:50:47 PM", "Customer", "CUSTOMER" },
                    { "25b977b1-60c6-46c2-9433-7bd17c819757", "22/09/2023 2:50:47 PM", "CarOwner", "CAROWNER" },
                    { "b8d3b6a4-2e37-4a71-9e20-ddab2277ba7c", "22/09/2023 2:50:47 PM", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8a3fe731-a014-4ac0-940f-cb461d84cf2f", 0, null, "f082f2b2-dbce-4266-9472-b4ffe3915709", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEN3sIL+je2n6sojXG8QeEY5T+GWEJHIJ4kwcLBKMTG39Em9DpFq3mUZS0rW8aHOY+A==", null, null, false, "fb574091-5b65-469e-b677-5f1309a541f3", false, "customer@email.com", 0m },
                    { "8ef93d3f-c02b-4c50-a740-a4d1d0832296", 0, null, "a43e0ffa-20b1-467e-9b0d-66bf4633e435", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEELzM0gGCgAvYUM1+PpjkXS1CvbJI1izkFl5Rexo91wiAOXMQ9s1IIR8iTe8uMIXmw==", null, null, false, "c6407df9-c542-4e0a-8597-997a5254755e", false, "carOwner@email.com", 0m },
                    { "91205127-10e4-45b0-be2e-f3c0b94fa16e", 0, null, "bad21ecf-a393-4cbd-8810-985047bdf578", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEJm3L05AqVwieHu/W6Iq2uWagDq7Xp+NjDm2cQYlEY25sGDWfm5/fSNi3QG4+U+OzQ==", null, null, false, "b1acb046-aaa2-4504-b885-71a717764480", false, "admin@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "097d30ac-85e3-415a-b14b-256e9bc2cbe6", "8a3fe731-a014-4ac0-940f-cb461d84cf2f" },
                    { "25b977b1-60c6-46c2-9433-7bd17c819757", "8ef93d3f-c02b-4c50-a740-a4d1d0832296" },
                    { "b8d3b6a4-2e37-4a71-9e20-ddab2277ba7c", "91205127-10e4-45b0-be2e-f3c0b94fa16e" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("49057f0e-a5f3-4e87-8632-b83f2f180dc5"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "8ef93d3f-c02b-4c50-a740-a4d1d0832296", "White", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4701), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4701) },
                    { new Guid("507d2b9b-b1d0-4845-8e10-454288d883f6"), "USB, Sunroof", 3, 45.0m, 3, "8ef93d3f-c02b-4c50-a740-a4d1d0832296", "Red", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4682), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4683) },
                    { new Guid("6a20603d-67a5-41e9-adfc-b6193ab9fb6e"), "DVD, Leather Seats", 4, 60.0m, 4, "8ef93d3f-c02b-4c50-a740-a4d1d0832296", "Black", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4688), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4689) },
                    { new Guid("9be5dfc5-b55e-453c-a91f-c6377362f2c2"), "GPS, Bluetooth", 2, 50.0m, 2, "8ef93d3f-c02b-4c50-a740-a4d1d0832296", "Blue", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4658), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4660) },
                    { new Guid("be8778d9-4fb5-4f72-a519-2dad1c7ce67b"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "8ef93d3f-c02b-4c50-a740-a4d1d0832296", "Silver", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4694), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4694) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("46b5cc2c-e374-4244-bc02-33fc413354df"), new Guid("507d2b9b-b1d0-4845-8e10-454288d883f6"), new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4727), "Interior of Car1", true, new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4728), "car1_image2.jpg" },
                    { new Guid("a3e1e7fb-9cd2-41cc-9a92-33615c3fbaef"), new Guid("9be5dfc5-b55e-453c-a91f-c6377362f2c2"), new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4724), "Back view of Car1", true, new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4724), "car1_image2.jpg" },
                    { new Guid("c9ba9473-3fee-4462-a855-c450a7b096e4"), new Guid("6a20603d-67a5-41e9-adfc-b6193ab9fb6e"), new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4730), "Front view of Car2", true, new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4730), "car2_image1.jpg" },
                    { new Guid("cf0c67e7-717b-425c-9baf-ef1db2c5328a"), new Guid("9be5dfc5-b55e-453c-a91f-c6377362f2c2"), new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4716), "Front view of Car1", true, new DateTime(2023, 9, 22, 14, 50, 47, 285, DateTimeKind.Local).AddTicks(4716), "car1_image1.jpg" }
                });
        }
    }
}
