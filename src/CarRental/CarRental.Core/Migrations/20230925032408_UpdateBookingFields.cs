﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class UpdateBookingFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4335fafa-ecd7-4ab0-8b50-7f5114d10882", "8072d963-65ed-468b-9e41-8cfd5ec830d7" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "ab1fbc30-00da-4581-99ae-79ae5be36f3b", "d01587bd-621c-449b-b4e3-4036d46af9da" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "0c39e186-f01b-4470-a428-7cb795fcc24d", "e3828257-0bc0-4f7e-9290-59fd14822aba" });

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("0c8ed75d-7816-463a-aef6-5bb9de42d7c9"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("11ca7f01-e617-4462-a31e-e834ceee4399"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("6e050c07-7cd3-4fb5-a2cc-89e99462253d"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("89d06c5d-8fbd-4a0e-a580-a725e71245d3"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("5058a0d2-8aa3-45a8-9fd6-c6df06d5b36e"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("f4273f29-6d5e-4d9a-85db-e42bc3a7742a"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0c39e186-f01b-4470-a428-7cb795fcc24d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4335fafa-ecd7-4ab0-8b50-7f5114d10882");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ab1fbc30-00da-4581-99ae-79ae5be36f3b");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8072d963-65ed-468b-9e41-8cfd5ec830d7");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "d01587bd-621c-449b-b4e3-4036d46af9da");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("64d725a1-a9ad-487f-8568-8e844ce08871"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("acd51528-4157-4afe-b061-bdb9729b9df2"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "e3828257-0bc0-4f7e-9290-59fd14822aba");

            migrationBuilder.DropColumn(
                name: "DriverInformation",
                table: "Bookings");

            migrationBuilder.AddColumn<string>(
                name: "DriverCity",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverCityCode",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DriverDateOfBirth",
                table: "Bookings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverDetailAddress",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverDistrict",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverDistrictCode",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverDrivingLicense",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverName",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverNationalIDNo",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverPhoneNo",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverWard",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverWardCode",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "54d6d475-1cbb-425e-9f43-be6387d42cb2", "25/09/2023 10:24:03 AM", "CarOwner", "CAROWNER" },
                    { "af715ea6-eb0f-4f2a-bccb-46e2279881f9", "25/09/2023 10:24:03 AM", "Admin", "ADMIN" },
                    { "ba401119-0344-476c-a421-cc5174f3fd82", "25/09/2023 10:24:03 AM", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8a2d4f09-9726-4736-a82d-858ca314e23f", 0, null, "d424adeb-8fe8-4ac9-9c54-e1a0513d3021", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEJgnEolc3/b46mRQUamQRRFHb66Rgr/L0lIEtWIXzKFWxSqz7DWj9TrW61iNYt23Jw==", null, null, false, "4c8ccfd5-4beb-4314-b84c-ca8ae98e8b6e", false, "admin@email.com", 0m },
                    { "97dabfbc-c4d5-4ccc-a459-a56182d69be5", 0, null, "61e10821-0718-4e10-9bfa-4e08e8683226", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEItefuhvnhxzovrUdhMtb/6wy+awXrkq6ZHdXxUW6ikMZvJB3mNzviaadpt14HY/0Q==", null, null, false, "cc46ad89-eaf3-4669-8eb6-1375dc4fb666", false, "carOwner@email.com", 0m },
                    { "c86f8498-b8f9-414f-a1a2-a9590fb2b39e", 0, null, "a811ce25-9742-4f49-b8c5-4f56a98608b7", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEB4tUghhQ3uKO92tWdFykec+vYiNci1gSzYeBZGP/dIc2DQgYWIoogi7ZqJ2HZxuoA==", null, null, false, "db3b189b-ed8d-4cbc-9739-385048fa9c6a", false, "customer@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "af715ea6-eb0f-4f2a-bccb-46e2279881f9", "8a2d4f09-9726-4736-a82d-858ca314e23f" },
                    { "54d6d475-1cbb-425e-9f43-be6387d42cb2", "97dabfbc-c4d5-4ccc-a459-a56182d69be5" },
                    { "ba401119-0344-476c-a421-cc5174f3fd82", "c86f8498-b8f9-414f-a1a2-a9590fb2b39e" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("2a8298f1-39c0-4ece-ab63-caaa721cf0bd"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "White", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7893), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7894) },
                    { new Guid("5479910e-f404-48a8-809f-64fa2d8ffa08"), "USB, Sunroof", 3, 45.0m, 3, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Red", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7875), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7875) },
                    { new Guid("907113df-7c80-4b3a-be31-299c47ce6111"), "GPS, Bluetooth", 2, 50.0m, 2, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Blue", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7858), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7860) },
                    { new Guid("a69b6e1c-ee54-475e-acb0-9c99f1aabd3e"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Silver", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7886), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7887) },
                    { new Guid("b3db6c7a-8c7d-427a-8c24-74d8c726731b"), "DVD, Leather Seats", 4, 60.0m, 4, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Black", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7881), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7881) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("0920bbb5-de31-459e-b785-c6c07b8aa53c"), new Guid("b3db6c7a-8c7d-427a-8c24-74d8c726731b"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7913), "Front view of Car2", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7913), "car2_image1.jpg" },
                    { new Guid("1e45120d-322e-4fa6-8e3f-84717c7389d1"), new Guid("907113df-7c80-4b3a-be31-299c47ce6111"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7905), "Back view of Car1", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7905), "car1_image2.jpg" },
                    { new Guid("42965aa7-562e-4334-b7bc-3506065f9c93"), new Guid("5479910e-f404-48a8-809f-64fa2d8ffa08"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7908), "Interior of Car1", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7908), "car1_image2.jpg" },
                    { new Guid("9f8e4802-4258-4502-bf9a-0eadec0ee717"), new Guid("907113df-7c80-4b3a-be31-299c47ce6111"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7901), "Front view of Car1", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7901), "car1_image1.jpg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "af715ea6-eb0f-4f2a-bccb-46e2279881f9", "8a2d4f09-9726-4736-a82d-858ca314e23f" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "54d6d475-1cbb-425e-9f43-be6387d42cb2", "97dabfbc-c4d5-4ccc-a459-a56182d69be5" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "ba401119-0344-476c-a421-cc5174f3fd82", "c86f8498-b8f9-414f-a1a2-a9590fb2b39e" });

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("0920bbb5-de31-459e-b785-c6c07b8aa53c"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("1e45120d-322e-4fa6-8e3f-84717c7389d1"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("42965aa7-562e-4334-b7bc-3506065f9c93"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("9f8e4802-4258-4502-bf9a-0eadec0ee717"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("2a8298f1-39c0-4ece-ab63-caaa721cf0bd"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("a69b6e1c-ee54-475e-acb0-9c99f1aabd3e"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "54d6d475-1cbb-425e-9f43-be6387d42cb2");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "af715ea6-eb0f-4f2a-bccb-46e2279881f9");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ba401119-0344-476c-a421-cc5174f3fd82");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8a2d4f09-9726-4736-a82d-858ca314e23f");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c86f8498-b8f9-414f-a1a2-a9590fb2b39e");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("5479910e-f404-48a8-809f-64fa2d8ffa08"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("907113df-7c80-4b3a-be31-299c47ce6111"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("b3db6c7a-8c7d-427a-8c24-74d8c726731b"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "97dabfbc-c4d5-4ccc-a459-a56182d69be5");

            migrationBuilder.DropColumn(
                name: "DriverCity",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverCityCode",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverDateOfBirth",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverDetailAddress",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverDistrict",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverDistrictCode",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverDrivingLicense",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverName",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverNationalIDNo",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverPhoneNo",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverWard",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DriverWardCode",
                table: "Bookings");

            migrationBuilder.AddColumn<string>(
                name: "DriverInformation",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "0c39e186-f01b-4470-a428-7cb795fcc24d", "24/09/2023 1:01:42 AM", "CarOwner", "CAROWNER" },
                    { "4335fafa-ecd7-4ab0-8b50-7f5114d10882", "24/09/2023 1:01:42 AM", "Admin", "ADMIN" },
                    { "ab1fbc30-00da-4581-99ae-79ae5be36f3b", "24/09/2023 1:01:42 AM", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8072d963-65ed-468b-9e41-8cfd5ec830d7", 0, null, "0efe418f-b01f-4d79-9446-3910c980ce91", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEAzgDqWQSjGFzInLUkEGCt0iOFNnhXW4ALXvMvYpuM2jat7V2TgjK7NV/SsYWGBoBQ==", null, null, false, "64423939-94f5-4a7f-b7d9-0cc5030fa6d9", false, "admin@email.com", 0m },
                    { "d01587bd-621c-449b-b4e3-4036d46af9da", 0, null, "a7e7b9f4-f0ed-437a-9033-0fd6dd72c2bd", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAELo5ETvuk80GTPHD8HX4a7KhJt+SmkG5pS5CONL7Vnn7a4rhNM0T1YVmbTCYlA8TVg==", null, null, false, "3b122cf8-c809-4d0e-b59f-1a0db0456712", false, "customer@email.com", 0m },
                    { "e3828257-0bc0-4f7e-9290-59fd14822aba", 0, null, "d9d7901f-5394-4487-8b1a-da7c1849ae8a", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEB3/tZfJLAzX9aQ1JINh7VXUZcXHAjLIn33IfDSlhT7IrAu/7rLg+yWWEIdktbgcfw==", null, null, false, "840f749d-4172-4059-8b57-43946df5e255", false, "carOwner@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "4335fafa-ecd7-4ab0-8b50-7f5114d10882", "8072d963-65ed-468b-9e41-8cfd5ec830d7" },
                    { "ab1fbc30-00da-4581-99ae-79ae5be36f3b", "d01587bd-621c-449b-b4e3-4036d46af9da" },
                    { "0c39e186-f01b-4470-a428-7cb795fcc24d", "e3828257-0bc0-4f7e-9290-59fd14822aba" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"), "GPS, Bluetooth", 2, 50.0m, 2, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Blue", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(805), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(810) },
                    { new Guid("5058a0d2-8aa3-45a8-9fd6-c6df06d5b36e"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "e3828257-0bc0-4f7e-9290-59fd14822aba", "White", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(848), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(848) },
                    { new Guid("64d725a1-a9ad-487f-8568-8e844ce08871"), "USB, Sunroof", 3, 45.0m, 3, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Red", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(825), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(826) },
                    { new Guid("acd51528-4157-4afe-b061-bdb9729b9df2"), "DVD, Leather Seats", 4, 60.0m, 4, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Black", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(836), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(837) },
                    { new Guid("f4273f29-6d5e-4d9a-85db-e42bc3a7742a"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "e3828257-0bc0-4f7e-9290-59fd14822aba", "Silver", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(842), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(842) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("0c8ed75d-7816-463a-aef6-5bb9de42d7c9"), new Guid("acd51528-4157-4afe-b061-bdb9729b9df2"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(868), "Front view of Car2", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(869), "car2_image1.jpg" },
                    { new Guid("11ca7f01-e617-4462-a31e-e834ceee4399"), new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(863), "Back view of Car1", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(863), "car1_image2.jpg" },
                    { new Guid("6e050c07-7cd3-4fb5-a2cc-89e99462253d"), new Guid("64d725a1-a9ad-487f-8568-8e844ce08871"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(866), "Interior of Car1", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(866), "car1_image2.jpg" },
                    { new Guid("89d06c5d-8fbd-4a0e-a580-a725e71245d3"), new Guid("2d5dcae6-2d2f-4fd4-9529-da586c91a54b"), new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(856), "Front view of Car1", true, new DateTime(2023, 9, 24, 1, 1, 42, 570, DateTimeKind.Local).AddTicks(856), "car1_image1.jpg" }
                });
        }
    }
}
