﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class AddStopRentingFieldToCarModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "aca00a67-0bd5-4a76-b72c-778c62097fff", "8ba9dc72-e968-4271-97dd-f3735f9125c5" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "3db45eff-c7d0-42f2-9cba-d69b3f2f3993", "91bb95f3-6fec-483b-af19-26d5eca1397c" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "92182802-f3da-4256-a8e3-87ed4f661a16", "da836098-5746-4fac-a9ec-cff241a2f5da" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3db45eff-c7d0-42f2-9cba-d69b3f2f3993");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "92182802-f3da-4256-a8e3-87ed4f661a16");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "aca00a67-0bd5-4a76-b72c-778c62097fff");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8ba9dc72-e968-4271-97dd-f3735f9125c5");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "91bb95f3-6fec-483b-af19-26d5eca1397c");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "da836098-5746-4fac-a9ec-cff241a2f5da");

            migrationBuilder.AddColumn<bool>(
                name: "IsStopRent",
                table: "Cars",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "234FA7FE-6C0B-4E16-9D94-3CC0EC9D8114", "02/10/2023 10:33:04 AM", "Customer", "CUSTOMER" },
                    { "BA07197E-631B-4E86-80F0-A167CB4FD6C1", "02/10/2023 10:33:04 AM", "Admin", "ADMIN" },
                    { "FB6DF12A-A1F9-43C9-B1D1-0B9085D7D285", "02/10/2023 10:33:04 AM", "CarOwner", "CAROWNER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "21B05714-45A3-48DB-930D-1C714FEC0E70", 0, null, "c5f5493e-3dc6-475f-8554-2de8af9ee3ca", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEKu+tCwcZmMFzEXtfFUZBr116l6oOq2Ng3sp2yWpRqfCGveK6sv1aIpf35RfTPqF6g==", null, null, false, "9cde6f8b-806f-4bf5-8b0c-d425e0e298f9", false, "customer@email.com", 0m },
                    { "874E5E74-B0CD-48AF-BD56-BD6366D17F2E", 0, null, "95c03d3d-fd06-47a8-b832-5cd7bca716bb", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAED0lt4Fw8f80OOa+Fe/F3nvj7A7ihlY27EATkSxRGxBcpJavhmCXTKWdnp6GVoq+PQ==", null, null, false, "d9fc66b7-5d55-4957-9c3f-86e6f56caa0d", false, "admin@email.com", 0m },
                    { "B4CA6EEF-2C72-415F-994B-A117A0E3F31C", 0, null, "8ce98848-a796-4786-8698-da3f3b730fa8", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEKvtiJGI9D5goOFUjqytaqDxqMz7sBaID7pXjUs/mha3vfzEqdY61jVTxT35mMjv6g==", null, null, false, "4c2521cf-7e59-434e-8eac-1f5d9bc1d720", false, "carOwner@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "234FA7FE-6C0B-4E16-9D94-3CC0EC9D8114", "21B05714-45A3-48DB-930D-1C714FEC0E70" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "BA07197E-631B-4E86-80F0-A167CB4FD6C1", "874E5E74-B0CD-48AF-BD56-BD6366D17F2E" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "FB6DF12A-A1F9-43C9-B1D1-0B9085D7D285", "B4CA6EEF-2C72-415F-994B-A117A0E3F31C" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "234FA7FE-6C0B-4E16-9D94-3CC0EC9D8114", "21B05714-45A3-48DB-930D-1C714FEC0E70" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "BA07197E-631B-4E86-80F0-A167CB4FD6C1", "874E5E74-B0CD-48AF-BD56-BD6366D17F2E" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "FB6DF12A-A1F9-43C9-B1D1-0B9085D7D285", "B4CA6EEF-2C72-415F-994B-A117A0E3F31C" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "234FA7FE-6C0B-4E16-9D94-3CC0EC9D8114");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "BA07197E-631B-4E86-80F0-A167CB4FD6C1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "FB6DF12A-A1F9-43C9-B1D1-0B9085D7D285");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "21B05714-45A3-48DB-930D-1C714FEC0E70");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "874E5E74-B0CD-48AF-BD56-BD6366D17F2E");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "B4CA6EEF-2C72-415F-994B-A117A0E3F31C");

            migrationBuilder.DropColumn(
                name: "IsStopRent",
                table: "Cars");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "3db45eff-c7d0-42f2-9cba-d69b3f2f3993", "28/09/2023 3:09:36 PM", "CarOwner", "CAROWNER" },
                    { "92182802-f3da-4256-a8e3-87ed4f661a16", "28/09/2023 3:09:36 PM", "Customer", "CUSTOMER" },
                    { "aca00a67-0bd5-4a76-b72c-778c62097fff", "28/09/2023 3:09:36 PM", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8ba9dc72-e968-4271-97dd-f3735f9125c5", 0, null, "747672b9-4f75-46dc-9f8c-c0955f6d95be", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAELhQ9p7k6wNDnaBfVF5ZuqdbY8RJFwVuqDLGSsgtKW9LzyYEju/Om0U3z+bMnDbdsQ==", null, null, false, "5dd000eb-a936-4f02-9206-335055210dae", false, "admin@email.com", 0m },
                    { "91bb95f3-6fec-483b-af19-26d5eca1397c", 0, null, "36ae90f5-0945-41af-803b-df9f5c44b1cd", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEBv4gSRmNQXq0ZjJYuOQcRQmOHDq4zzoDfZaJ0RXus8ffMqflRHPr/WQlykHtsVVrQ==", null, null, false, "be28d57e-443f-4d93-8161-77e9389ab5ef", false, "carOwner@email.com", 0m },
                    { "da836098-5746-4fac-a9ec-cff241a2f5da", 0, null, "62adaece-7221-44c7-82a7-0441c67c8b50", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEASgx40H+ntODNbNA9Ts14sQEnbtL4gZS4MJCx+HXIojBo8sTamFUOf4dUSYN40SQA==", null, null, false, "746537fd-8cfe-47c6-819e-db3fe5fbc06f", false, "customer@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "aca00a67-0bd5-4a76-b72c-778c62097fff", "8ba9dc72-e968-4271-97dd-f3735f9125c5" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "3db45eff-c7d0-42f2-9cba-d69b3f2f3993", "91bb95f3-6fec-483b-af19-26d5eca1397c" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "92182802-f3da-4256-a8e3-87ed4f661a16", "da836098-5746-4fac-a9ec-cff241a2f5da" });
        }
    }
}
