﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class SeedDataForBookingModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "af715ea6-eb0f-4f2a-bccb-46e2279881f9", "8a2d4f09-9726-4736-a82d-858ca314e23f" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "54d6d475-1cbb-425e-9f43-be6387d42cb2", "97dabfbc-c4d5-4ccc-a459-a56182d69be5" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "ba401119-0344-476c-a421-cc5174f3fd82", "c86f8498-b8f9-414f-a1a2-a9590fb2b39e" });

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("0920bbb5-de31-459e-b785-c6c07b8aa53c"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("1e45120d-322e-4fa6-8e3f-84717c7389d1"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("42965aa7-562e-4334-b7bc-3506065f9c93"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("9f8e4802-4258-4502-bf9a-0eadec0ee717"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("2a8298f1-39c0-4ece-ab63-caaa721cf0bd"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("a69b6e1c-ee54-475e-acb0-9c99f1aabd3e"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "54d6d475-1cbb-425e-9f43-be6387d42cb2");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "af715ea6-eb0f-4f2a-bccb-46e2279881f9");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ba401119-0344-476c-a421-cc5174f3fd82");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8a2d4f09-9726-4736-a82d-858ca314e23f");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c86f8498-b8f9-414f-a1a2-a9590fb2b39e");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("5479910e-f404-48a8-809f-64fa2d8ffa08"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("907113df-7c80-4b3a-be31-299c47ce6111"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("b3db6c7a-8c7d-427a-8c24-74d8c726731b"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "97dabfbc-c4d5-4ccc-a459-a56182d69be5");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "62d9fbc0-409a-46ee-af31-f3dc989408df", "25/09/2023 10:49:25 AM", "Admin", "ADMIN" },
                    { "7b39de24-c120-467c-b5dc-2febd21a9ff5", "25/09/2023 10:49:25 AM", "CarOwner", "CAROWNER" },
                    { "a4c61962-d81f-45a1-8365-6602098ccd9a", "25/09/2023 10:49:25 AM", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3", 0, null, "14910ddf-2ed4-4984-b3cf-a4d4c90ed1e2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEOZA+0lf0HP3csi5Nz2Zx8CmgAjpDpYbrkUV2MKOkNjJ/fMN/tmQ9bhGzOpsjij3Yw==", null, null, false, "5bf283d4-b7ce-467d-a400-b7ad52079ee3", false, "admin@email.com", 0m },
                    { "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", 0, null, "08fced0e-f918-4237-937b-7dd04fe3671d", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEG2FzG6SzmzujLmgp0bs8FIx1epkw8yvxSzn6mVqQdZudgme7ZGYcAlde7uC/S2MgA==", null, null, false, "a6898585-12c2-4ee0-ac42-85ac013826c3", false, "customer@email.com", 0m },
                    { "f67de744-795e-41d3-a208-65f1e4952681", 0, null, "b8c3a820-f2d4-4220-afc6-b225d895b284", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEAOdyvyAsTbYGntbrpxmrdNAT3TVUIORBw4jz10ekS9DdcXLMsm6dYqC0iHvMJ3XwQ==", null, null, false, "205847c5-6c48-44ec-8d03-42e9df3869a0", false, "carOwner@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "62d9fbc0-409a-46ee-af31-f3dc989408df", "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3" },
                    { "a4c61962-d81f-45a1-8365-6602098ccd9a", "7f1a09e0-ea33-4284-874f-3f9e0f5a4643" },
                    { "7b39de24-c120-467c-b5dc-2febd21a9ff5", "f67de744-795e-41d3-a208-65f1e4952681" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), "GPS, Bluetooth", 2, 50.0m, 2, "f67de744-795e-41d3-a208-65f1e4952681", "Blue", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5150), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5156) },
                    { new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"), "USB, Sunroof", 3, 45.0m, 3, "f67de744-795e-41d3-a208-65f1e4952681", "Red", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5176), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5176) },
                    { new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"), "DVD, Leather Seats", 4, 60.0m, 4, "f67de744-795e-41d3-a208-65f1e4952681", "Black", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5183), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5183) },
                    { new Guid("e6329ad7-27bf-49c3-bf01-db385cbad599"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "f67de744-795e-41d3-a208-65f1e4952681", "Silver", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5193), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5193) },
                    { new Guid("fb571300-e935-44da-846c-e76c0e3a6f47"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "f67de744-795e-41d3-a208-65f1e4952681", "White", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5201), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5202) }
                });

            migrationBuilder.InsertData(
                table: "Bookings",
                columns: new[] { "Id", "BookingNo", "CarId", "CreatedAt", "CustomerId", "DriverCity", "DriverCityCode", "DriverDateOfBirth", "DriverDetailAddress", "DriverDistrict", "DriverDistrictCode", "DriverDrivingLicense", "DriverName", "DriverNationalIDNo", "DriverPhoneNo", "DriverWard", "DriverWardCode", "EndDateTime", "IsActive", "PaymentMethod", "StartDateTime", "Status", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("34e343b8-f7ac-42f9-8ed3-4fe8985c8405"), "Booking004", new Guid("e6329ad7-27bf-49c3-bf01-db385cbad599"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5267), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City B", "C101", new DateTime(1992, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "101 Pine Street", "District A", "D101", "DL777777", "Bob Smith", "2468135790", "777-987-6543", "Ward D", "W101", new DateTime(2023, 9, 25, 14, 49, 25, 131, DateTimeKind.Utc).AddTicks(5269), true, "Credit Card", new DateTime(2023, 9, 25, 12, 49, 25, 131, DateTimeKind.Utc).AddTicks(5268), "Confirmed", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5267) },
                    { new Guid("51fd03b9-67cb-433f-9f59-f522f9da021e"), "Booking005", new Guid("fb571300-e935-44da-846c-e76c0e3a6f47"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5272), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City C", "C222", new DateTime(1988, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "222 Maple Street", "District B", "D222", "DL888888", "Eve Davis", "3698521470", "888-555-1212", "Ward E", "W222", new DateTime(2023, 9, 25, 17, 49, 25, 131, DateTimeKind.Utc).AddTicks(5274), true, "PayPal", new DateTime(2023, 9, 25, 15, 49, 25, 131, DateTimeKind.Utc).AddTicks(5274), "Pending", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5272) },
                    { new Guid("814447c9-d306-4519-a68b-3fe4692f601c"), "Booking002", new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5252), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City Z", "C456", new DateTime(1990, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "456 Elm Street", "District Y", "D456", "DL987654", "Jane Smith", "9876543210", "987-654-3210", "Ward B", "W456", new DateTime(2023, 9, 25, 8, 49, 25, 131, DateTimeKind.Utc).AddTicks(5256), true, "PayPal", new DateTime(2023, 9, 25, 6, 49, 25, 131, DateTimeKind.Utc).AddTicks(5255), "Pending", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5252) },
                    { new Guid("a7619a0f-e42c-4b12-87f7-43e66f79746f"), "Booking003", new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5261), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City W", "C789", new DateTime(1985, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "789 Oak Avenue", "District Z", "D789", "DL555555", "Alice Johnson", "1357924680", "555-123-4567", "Ward C", "W789", new DateTime(2023, 9, 25, 11, 49, 25, 131, DateTimeKind.Utc).AddTicks(5263), true, "Cash", new DateTime(2023, 9, 25, 9, 49, 25, 131, DateTimeKind.Utc).AddTicks(5263), "Confirmed", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5262) },
                    { new Guid("b7881ad9-b1bf-4641-9b81-1d11435f12bf"), "Booking001", new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5230), "7f1a09e0-ea33-4284-874f-3f9e0f5a4643", "City Y", "C123", new DateTime(1980, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "123 Main Street", "District X", "D123", "DL123456", "John Doe", "1234567890", "123-456-7890", "Ward A", "W123", new DateTime(2023, 9, 25, 5, 49, 25, 131, DateTimeKind.Utc).AddTicks(5233), true, "Credit Card", new DateTime(2023, 9, 25, 3, 49, 25, 131, DateTimeKind.Utc).AddTicks(5232), "Confirmed", new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5231) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("1c7b4c5e-b538-4423-9c91-7be0b79b8a4c"), new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5225), "Interior of Car1", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5225), "car1_image2.jpg" },
                    { new Guid("22246157-606c-4837-838d-8344764a6c96"), new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5210), "Front view of Car1", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5211), "car1_image1.jpg" },
                    { new Guid("5dae25f3-9884-48b0-955d-c75c7ad24aba"), new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5219), "Back view of Car1", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5220), "car1_image2.jpg" },
                    { new Guid("c87be9bf-43dc-44ba-ba4c-9cff1d3333a9"), new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"), new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5227), "Front view of Car2", true, new DateTime(2023, 9, 25, 10, 49, 25, 131, DateTimeKind.Local).AddTicks(5228), "car2_image1.jpg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "62d9fbc0-409a-46ee-af31-f3dc989408df", "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "a4c61962-d81f-45a1-8365-6602098ccd9a", "7f1a09e0-ea33-4284-874f-3f9e0f5a4643" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "7b39de24-c120-467c-b5dc-2febd21a9ff5", "f67de744-795e-41d3-a208-65f1e4952681" });

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("34e343b8-f7ac-42f9-8ed3-4fe8985c8405"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("51fd03b9-67cb-433f-9f59-f522f9da021e"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("814447c9-d306-4519-a68b-3fe4692f601c"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("a7619a0f-e42c-4b12-87f7-43e66f79746f"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("b7881ad9-b1bf-4641-9b81-1d11435f12bf"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("1c7b4c5e-b538-4423-9c91-7be0b79b8a4c"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("22246157-606c-4837-838d-8344764a6c96"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("5dae25f3-9884-48b0-955d-c75c7ad24aba"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("c87be9bf-43dc-44ba-ba4c-9cff1d3333a9"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "62d9fbc0-409a-46ee-af31-f3dc989408df");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7b39de24-c120-467c-b5dc-2febd21a9ff5");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a4c61962-d81f-45a1-8365-6602098ccd9a");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "165310c1-6b6d-47c7-952e-0a4fc3c5ebf3");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "7f1a09e0-ea33-4284-874f-3f9e0f5a4643");

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("3f89a580-c2a2-44c8-9698-a9288c917183"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("8c2dea1d-9271-4f53-ad7e-9448b8860dde"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9862190c-83ce-481c-b29c-a6a05e825b08"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("e6329ad7-27bf-49c3-bf01-db385cbad599"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("fb571300-e935-44da-846c-e76c0e3a6f47"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f67de744-795e-41d3-a208-65f1e4952681");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "54d6d475-1cbb-425e-9f43-be6387d42cb2", "25/09/2023 10:24:03 AM", "CarOwner", "CAROWNER" },
                    { "af715ea6-eb0f-4f2a-bccb-46e2279881f9", "25/09/2023 10:24:03 AM", "Admin", "ADMIN" },
                    { "ba401119-0344-476c-a421-cc5174f3fd82", "25/09/2023 10:24:03 AM", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8a2d4f09-9726-4736-a82d-858ca314e23f", 0, null, "d424adeb-8fe8-4ac9-9c54-e1a0513d3021", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEJgnEolc3/b46mRQUamQRRFHb66Rgr/L0lIEtWIXzKFWxSqz7DWj9TrW61iNYt23Jw==", null, null, false, "4c8ccfd5-4beb-4314-b84c-ca8ae98e8b6e", false, "admin@email.com", 0m },
                    { "97dabfbc-c4d5-4ccc-a459-a56182d69be5", 0, null, "61e10821-0718-4e10-9bfa-4e08e8683226", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEItefuhvnhxzovrUdhMtb/6wy+awXrkq6ZHdXxUW6ikMZvJB3mNzviaadpt14HY/0Q==", null, null, false, "cc46ad89-eaf3-4669-8eb6-1375dc4fb666", false, "carOwner@email.com", 0m },
                    { "c86f8498-b8f9-414f-a1a2-a9590fb2b39e", 0, null, "a811ce25-9742-4f49-b8c5-4f56a98608b7", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEB4tUghhQ3uKO92tWdFykec+vYiNci1gSzYeBZGP/dIc2DQgYWIoogi7ZqJ2HZxuoA==", null, null, false, "db3b189b-ed8d-4cbc-9739-385048fa9c6a", false, "customer@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "af715ea6-eb0f-4f2a-bccb-46e2279881f9", "8a2d4f09-9726-4736-a82d-858ca314e23f" },
                    { "54d6d475-1cbb-425e-9f43-be6387d42cb2", "97dabfbc-c4d5-4ccc-a459-a56182d69be5" },
                    { "ba401119-0344-476c-a421-cc5174f3fd82", "c86f8498-b8f9-414f-a1a2-a9590fb2b39e" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("2a8298f1-39c0-4ece-ab63-caaa721cf0bd"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "White", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7893), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7894) },
                    { new Guid("5479910e-f404-48a8-809f-64fa2d8ffa08"), "USB, Sunroof", 3, 45.0m, 3, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Red", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7875), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7875) },
                    { new Guid("907113df-7c80-4b3a-be31-299c47ce6111"), "GPS, Bluetooth", 2, 50.0m, 2, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Blue", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7858), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7860) },
                    { new Guid("a69b6e1c-ee54-475e-acb0-9c99f1aabd3e"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Silver", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7886), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7887) },
                    { new Guid("b3db6c7a-8c7d-427a-8c24-74d8c726731b"), "DVD, Leather Seats", 4, 60.0m, 4, "97dabfbc-c4d5-4ccc-a459-a56182d69be5", "Black", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7881), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7881) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("0920bbb5-de31-459e-b785-c6c07b8aa53c"), new Guid("b3db6c7a-8c7d-427a-8c24-74d8c726731b"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7913), "Front view of Car2", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7913), "car2_image1.jpg" },
                    { new Guid("1e45120d-322e-4fa6-8e3f-84717c7389d1"), new Guid("907113df-7c80-4b3a-be31-299c47ce6111"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7905), "Back view of Car1", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7905), "car1_image2.jpg" },
                    { new Guid("42965aa7-562e-4334-b7bc-3506065f9c93"), new Guid("5479910e-f404-48a8-809f-64fa2d8ffa08"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7908), "Interior of Car1", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7908), "car1_image2.jpg" },
                    { new Guid("9f8e4802-4258-4502-bf9a-0eadec0ee717"), new Guid("907113df-7c80-4b3a-be31-299c47ce6111"), new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7901), "Front view of Car1", true, new DateTime(2023, 9, 25, 10, 24, 3, 331, DateTimeKind.Local).AddTicks(7901), "car1_image1.jpg" }
                });
        }
    }
}
