﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class ChangeNullableInWalletHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WalletTransactionHistories_Bookings_BookingId",
                table: "WalletTransactionHistories");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "309711f2-beac-448c-8426-c80b94548955", "0aec01a6-cb9b-481e-b927-a868a9a58aa5" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc", "a6a21a3f-1f85-44a2-9973-4fd52182f9e7" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb", "d6737bea-11f3-45a4-b548-2ac10dfbd277" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "309711f2-beac-448c-8426-c80b94548955");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0aec01a6-cb9b-481e-b927-a868a9a58aa5");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a6a21a3f-1f85-44a2-9973-4fd52182f9e7");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "d6737bea-11f3-45a4-b548-2ac10dfbd277");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionType",
                table: "WalletTransactionHistories",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionDescription",
                table: "WalletTransactionHistories",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "BookingId",
                table: "WalletTransactionHistories",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "3db45eff-c7d0-42f2-9cba-d69b3f2f3993", "28/09/2023 3:09:36 PM", "CarOwner", "CAROWNER" },
                    { "92182802-f3da-4256-a8e3-87ed4f661a16", "28/09/2023 3:09:36 PM", "Customer", "CUSTOMER" },
                    { "aca00a67-0bd5-4a76-b72c-778c62097fff", "28/09/2023 3:09:36 PM", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "8ba9dc72-e968-4271-97dd-f3735f9125c5", 0, null, "747672b9-4f75-46dc-9f8c-c0955f6d95be", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAELhQ9p7k6wNDnaBfVF5ZuqdbY8RJFwVuqDLGSsgtKW9LzyYEju/Om0U3z+bMnDbdsQ==", null, null, false, "5dd000eb-a936-4f02-9206-335055210dae", false, "admin@email.com", 0m },
                    { "91bb95f3-6fec-483b-af19-26d5eca1397c", 0, null, "36ae90f5-0945-41af-803b-df9f5c44b1cd", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEBv4gSRmNQXq0ZjJYuOQcRQmOHDq4zzoDfZaJ0RXus8ffMqflRHPr/WQlykHtsVVrQ==", null, null, false, "be28d57e-443f-4d93-8161-77e9389ab5ef", false, "carOwner@email.com", 0m },
                    { "da836098-5746-4fac-a9ec-cff241a2f5da", 0, null, "62adaece-7221-44c7-82a7-0441c67c8b50", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEASgx40H+ntODNbNA9Ts14sQEnbtL4gZS4MJCx+HXIojBo8sTamFUOf4dUSYN40SQA==", null, null, false, "746537fd-8cfe-47c6-819e-db3fe5fbc06f", false, "customer@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "aca00a67-0bd5-4a76-b72c-778c62097fff", "8ba9dc72-e968-4271-97dd-f3735f9125c5" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "3db45eff-c7d0-42f2-9cba-d69b3f2f3993", "91bb95f3-6fec-483b-af19-26d5eca1397c" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "92182802-f3da-4256-a8e3-87ed4f661a16", "da836098-5746-4fac-a9ec-cff241a2f5da" });

            migrationBuilder.AddForeignKey(
                name: "FK_WalletTransactionHistories_Bookings_BookingId",
                table: "WalletTransactionHistories",
                column: "BookingId",
                principalTable: "Bookings",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WalletTransactionHistories_Bookings_BookingId",
                table: "WalletTransactionHistories");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "aca00a67-0bd5-4a76-b72c-778c62097fff", "8ba9dc72-e968-4271-97dd-f3735f9125c5" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "3db45eff-c7d0-42f2-9cba-d69b3f2f3993", "91bb95f3-6fec-483b-af19-26d5eca1397c" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "92182802-f3da-4256-a8e3-87ed4f661a16", "da836098-5746-4fac-a9ec-cff241a2f5da" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3db45eff-c7d0-42f2-9cba-d69b3f2f3993");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "92182802-f3da-4256-a8e3-87ed4f661a16");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "aca00a67-0bd5-4a76-b72c-778c62097fff");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8ba9dc72-e968-4271-97dd-f3735f9125c5");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "91bb95f3-6fec-483b-af19-26d5eca1397c");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "da836098-5746-4fac-a9ec-cff241a2f5da");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionType",
                table: "WalletTransactionHistories",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "TransactionDescription",
                table: "WalletTransactionHistories",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "BookingId",
                table: "WalletTransactionHistories",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "309711f2-beac-448c-8426-c80b94548955", "28/09/2023 2:20:27 PM", "Customer", "CUSTOMER" },
                    { "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb", "28/09/2023 2:20:27 PM", "Admin", "ADMIN" },
                    { "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc", "28/09/2023 2:20:27 PM", "CarOwner", "CAROWNER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "0aec01a6-cb9b-481e-b927-a868a9a58aa5", 0, null, "04526217-9bae-450a-a894-c5719427b987", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEBqbcKF1xdTVEvH/YSKBmKYqsFZcLEx3VbC1gHVABd3/IPWR6KfLyNDSuoJI0M9wHQ==", null, null, false, "020ab84e-aa53-44ae-b8e3-383fad4a31d7", false, "customer@email.com", 0m },
                    { "a6a21a3f-1f85-44a2-9973-4fd52182f9e7", 0, null, "e96ee02b-8b07-44f8-9b08-af6460d390ec", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEEHCfrORzZV2YOLM441L8rp2EXJt8bjKRAqA87WgFq52ink5+kcLoThhe8UJBPSKdA==", null, null, false, "969092af-4820-4103-ad89-75f673bea599", false, "carOwner@email.com", 0m },
                    { "d6737bea-11f3-45a4-b548-2ac10dfbd277", 0, null, "80a5dee8-ab82-4d99-9dde-cfed37384d9b", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEM7YNwDRq0vUBloYNXp7/VjTfcrdQ+ufn0YOx0AUAQtfstqQ0/yrqiTgs+qvsPnwPg==", null, null, false, "91698ef5-9ded-4bbd-9d84-e7066452afcc", false, "admin@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "309711f2-beac-448c-8426-c80b94548955", "0aec01a6-cb9b-481e-b927-a868a9a58aa5" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc", "a6a21a3f-1f85-44a2-9973-4fd52182f9e7" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb", "d6737bea-11f3-45a4-b548-2ac10dfbd277" });

            migrationBuilder.AddForeignKey(
                name: "FK_WalletTransactionHistories_Bookings_BookingId",
                table: "WalletTransactionHistories",
                column: "BookingId",
                principalTable: "Bookings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
