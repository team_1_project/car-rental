﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarRental.Core.Migrations
{
    public partial class RemoveSeedDataAndAddMoreFieldsToBooking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_CustomerId",
                table: "Bookings");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "10ac869e-9306-420e-a584-a0ab2b522d51", "0754ad7e-7a89-498e-b054-016e03e7b6d1" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "b6e2f941-bce9-4057-8ee3-14da97a9ec0f", "2e3ab1e8-7b4f-4343-92e1-98936af87e38" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7", "5371c83d-e0bb-48eb-9bb2-da49162d0fe9" });

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("16cd8019-94a8-4b44-b850-529a9e50d9dc"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("575fc48e-f5df-4594-bf4c-78c526df73d0"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("92274bf6-66d5-415b-a180-21225f8a60ed"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("ec968489-5d89-408d-bdda-efffe84ca736"));

            migrationBuilder.DeleteData(
                table: "Bookings",
                keyColumn: "Id",
                keyValue: new Guid("f55d9385-f1bf-444f-a9ef-364d086ccf84"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("309f44fa-a3f5-4fe9-8d42-c95ee741da15"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("5bd0a58c-e9b3-4ee0-a16d-dfc0c1079597"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("9825bfdd-b12d-4a96-ae3b-61b243a29926"));

            migrationBuilder.DeleteData(
                table: "CarImages",
                keyColumn: "Id",
                keyValue: new Guid("afab0ac4-8563-4bf5-8bf5-ec724f7f94ad"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("851fa2bf-afe1-4254-b8c8-8c9dfa41932d"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9e5333e0-1b60-4904-98f9-1e95aff995bb"));

            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "10ac869e-9306-420e-a584-a0ab2b522d51");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b6e2f941-bce9-4057-8ee3-14da97a9ec0f");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0754ad7e-7a89-498e-b054-016e03e7b6d1");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "5371c83d-e0bb-48eb-9bb2-da49162d0fe9");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2e3ab1e8-7b4f-4343-92e1-98936af87e38");

            migrationBuilder.AddColumn<Guid>(
                name: "BookingId",
                table: "WalletTransactionHistories",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Feedbacks",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "TransmissionType",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "TermsOfUser",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LicensePlate",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Images",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FuelType",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "DetailAddress",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Color",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "AdditionalFunction",
                table: "Cars",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Url",
                table: "CarImages",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "CarImages",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "PaymentMethod",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "DriverWardCode",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverWard",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverPhoneNo",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverNationalIDNo",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverName",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDrivingLicense",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDistrictCode",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDistrict",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDetailAddress",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverCityCode",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverCity",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CustomerId",
                table: "Bookings",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "BookingNo",
                table: "Bookings",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CityCode",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "Bookings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DetailAddress",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "District",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DistrictCode",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DrivingLicense",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NationalIDNo",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNo",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ward",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WardCode",
                table: "Bookings",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "309711f2-beac-448c-8426-c80b94548955", "28/09/2023 2:20:27 PM", "Customer", "CUSTOMER" },
                    { "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb", "28/09/2023 2:20:27 PM", "Admin", "ADMIN" },
                    { "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc", "28/09/2023 2:20:27 PM", "CarOwner", "CAROWNER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "0aec01a6-cb9b-481e-b927-a868a9a58aa5", 0, null, "04526217-9bae-450a-a894-c5719427b987", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAEBqbcKF1xdTVEvH/YSKBmKYqsFZcLEx3VbC1gHVABd3/IPWR6KfLyNDSuoJI0M9wHQ==", null, null, false, "020ab84e-aa53-44ae-b8e3-383fad4a31d7", false, "customer@email.com", 0m },
                    { "a6a21a3f-1f85-44a2-9973-4fd52182f9e7", 0, null, "e96ee02b-8b07-44f8-9b08-af6460d390ec", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEEHCfrORzZV2YOLM441L8rp2EXJt8bjKRAqA87WgFq52ink5+kcLoThhe8UJBPSKdA==", null, null, false, "969092af-4820-4103-ad89-75f673bea599", false, "carOwner@email.com", 0m },
                    { "d6737bea-11f3-45a4-b548-2ac10dfbd277", 0, null, "80a5dee8-ab82-4d99-9dde-cfed37384d9b", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEM7YNwDRq0vUBloYNXp7/VjTfcrdQ+ufn0YOx0AUAQtfstqQ0/yrqiTgs+qvsPnwPg==", null, null, false, "91698ef5-9ded-4bbd-9d84-e7066452afcc", false, "admin@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "309711f2-beac-448c-8426-c80b94548955", "0aec01a6-cb9b-481e-b927-a868a9a58aa5" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc", "a6a21a3f-1f85-44a2-9973-4fd52182f9e7" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb", "d6737bea-11f3-45a4-b548-2ac10dfbd277" });

            migrationBuilder.CreateIndex(
                name: "IX_WalletTransactionHistories_BookingId",
                table: "WalletTransactionHistories",
                column: "BookingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_CustomerId",
                table: "Bookings",
                column: "CustomerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WalletTransactionHistories_Bookings_BookingId",
                table: "WalletTransactionHistories",
                column: "BookingId",
                principalTable: "Bookings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_CustomerId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_WalletTransactionHistories_Bookings_BookingId",
                table: "WalletTransactionHistories");

            migrationBuilder.DropIndex(
                name: "IX_WalletTransactionHistories_BookingId",
                table: "WalletTransactionHistories");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "309711f2-beac-448c-8426-c80b94548955", "0aec01a6-cb9b-481e-b927-a868a9a58aa5" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc", "a6a21a3f-1f85-44a2-9973-4fd52182f9e7" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb", "d6737bea-11f3-45a4-b548-2ac10dfbd277" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "309711f2-beac-448c-8426-c80b94548955");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4ec1bc9f-1596-4d81-995e-d9a8a2ad27cb");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d7d5605a-3a5a-44fb-a9ba-6542a503d1cc");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0aec01a6-cb9b-481e-b927-a868a9a58aa5");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a6a21a3f-1f85-44a2-9973-4fd52182f9e7");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "d6737bea-11f3-45a4-b548-2ac10dfbd277");

            migrationBuilder.DropColumn(
                name: "BookingId",
                table: "WalletTransactionHistories");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "CityCode",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DetailAddress",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "District",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DistrictCode",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "DrivingLicense",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "NationalIDNo",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "PhoneNo",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "Ward",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "WardCode",
                table: "Bookings");

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Feedbacks",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "TransmissionType",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "TermsOfUser",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LicensePlate",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "Images",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FuelType",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "DetailAddress",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Color",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "AdditionalFunction",
                table: "Cars",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Url",
                table: "CarImages",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "CarImages",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "PaymentMethod",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "DriverWardCode",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverWard",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverPhoneNo",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverNationalIDNo",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverName",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDrivingLicense",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDistrictCode",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDistrict",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverDetailAddress",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverCityCode",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverCity",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CustomerId",
                table: "Bookings",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookingNo",
                table: "Bookings",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "10ac869e-9306-420e-a584-a0ab2b522d51", "27/09/2023 9:44:05 AM", "Admin", "ADMIN" },
                    { "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7", "27/09/2023 9:44:05 AM", "Customer", "CUSTOMER" },
                    { "b6e2f941-bce9-4057-8ee3-14da97a9ec0f", "27/09/2023 9:44:05 AM", "CarOwner", "CAROWNER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AddressId", "ConcurrencyStamp", "DateOfBirth", "DetailAddress", "DrivingLicense", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NationalIDNo", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNo", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "Wallet" },
                values: new object[,]
                {
                    { "0754ad7e-7a89-498e-b054-016e03e7b6d1", 0, null, "8552dda0-5425-4b5e-a6cc-f4c23c1ed434", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "admin@email.com", true, true, null, null, null, "ADMIN@EMAIL.COM", "ADMIN@EMAIL.COM", "AQAAAAEAACcQAAAAEB5CKF5XOnF8THaT9J3Fv2B08mpdqO1KoQizOBgU6OVxWCksLt/m3gqMhAZttSU8wA==", null, null, false, "41c30945-0fd7-4b1b-990e-b6171e2d8d31", false, "admin@email.com", 0m },
                    { "2e3ab1e8-7b4f-4343-92e1-98936af87e38", 0, null, "6a4d12f1-9df1-43dd-846b-e4e4febf9d6f", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "carOwner@email.com", true, true, null, null, null, "CAROWNER@EMAIL.COM", "CAROWNER@EMAIL.COM", "AQAAAAEAACcQAAAAEGGarWQ6uWOdgSGyj7pZAGaX1aEcxGBi4cXB0St5IEDr5/2wqrxs0W/uMrDvLI9POw==", null, null, false, "4a00130e-1b7b-47a2-b5d9-44a1192546a1", false, "carOwner@email.com", 0m },
                    { "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", 0, null, "98a92998-eba7-490f-83aa-952acb8b569a", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "customer@email.com", true, true, null, null, null, "CUSTOMER@EMAIL.COM", "CUSTOMER@EMAIL.COM", "AQAAAAEAACcQAAAAECfvNHLolC7UaY9qtrfE1vM8ioV8HeYMtnEyzSAwp1aze1ZkJwI3SJzWSt7KIhMU7w==", null, null, false, "287d1243-734a-4f11-988b-a1c6bd91ce4e", false, "customer@email.com", 0m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "10ac869e-9306-420e-a584-a0ab2b522d51", "0754ad7e-7a89-498e-b054-016e03e7b6d1" },
                    { "b6e2f941-bce9-4057-8ee3-14da97a9ec0f", "2e3ab1e8-7b4f-4343-92e1-98936af87e38" },
                    { "5ee8c0f0-7ae5-4c67-86b7-1ae12bcfecf7", "5371c83d-e0bb-48eb-9bb2-da49162d0fe9" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalFunction", "AddressId", "BasePrice", "BrandModelId", "CarOwnerId", "Color", "CreatedAt", "Deposit", "Description", "DetailAddress", "FuelConsumption", "FuelType", "Images", "IsActive", "LicensePlate", "Mileage", "Name", "NumberOfSeat", "ProductionYear", "TermsOfUser", "TransmissionType", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), "GPS, Bluetooth", 2, 50.0m, 2, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Blue", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7672), 500.0m, "A comfortable sedan for daily use.", null, 8.5m, "Gasoline", "car1.jpg", true, "ABC123", 5000.0m, "Car1", 5, 2020, "No smoking, return with a full tank.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7687) },
                    { new Guid("851fa2bf-afe1-4254-b8c8-8c9dfa41932d"), "Navigation, Panoramic Roof", 5, 70.0m, 5, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Silver", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7733), 700.0m, "A luxury SUV for a comfortable ride.", null, 6.5m, "Diesel", "car4.jpg", true, "GHI012", 4000.0m, "Car4", 5, 2021, "No smoking, premium fuel only.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7734) },
                    { new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"), "USB, Sunroof", 3, 45.0m, 3, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Red", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7718), 450.0m, "A reliable compact car for city driving.", null, 7.0m, "Gasoline", "car2.jpg", true, "XYZ456", 6000.0m, "Car2", 4, 2019, "No pets, clean before returning.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7719) },
                    { new Guid("9e5333e0-1b60-4904-98f9-1e95aff995bb"), "Bluetooth, Leather Interior", 6, 75.0m, 6, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "White", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7766), 750.0m, "An elegant sedan for a luxurious experience.", null, 5.0m, "Hybrid", "car5.jpg", true, "JKL345", 3000.0m, "Car5", 4, 2020, "No pets, eco-friendly driving.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7767) },
                    { new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"), "DVD, Leather Seats", 4, 60.0m, 4, "2e3ab1e8-7b4f-4343-92e1-98936af87e38", "Black", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7727), 600.0m, "A spacious SUV for family trips.", null, 9.0m, "Gasoline", "car3.jpg", true, "DEF789", 2000.0m, "Car3", 7, 2022, "No off-road, clean after use.", "Automatic", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7727) }
                });

            migrationBuilder.InsertData(
                table: "Bookings",
                columns: new[] { "Id", "BookingNo", "CarId", "CreatedAt", "CustomerId", "DriverCity", "DriverCityCode", "DriverDateOfBirth", "DriverDetailAddress", "DriverDistrict", "DriverDistrictCode", "DriverDrivingLicense", "DriverName", "DriverNationalIDNo", "DriverPhoneNo", "DriverWard", "DriverWardCode", "EndDateTime", "IsActive", "PaymentMethod", "StartDateTime", "Status", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("16cd8019-94a8-4b44-b850-529a9e50d9dc"), "Booking002", new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7827), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City Z", "C456", new DateTime(1990, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "456 Elm Street", "District Y", "D456", "DL987654", "Jane Smith", "9876543210", "987-654-3210", "Ward B", "W456", new DateTime(2023, 9, 27, 7, 44, 5, 831, DateTimeKind.Utc).AddTicks(7831), true, "PayPal", new DateTime(2023, 9, 27, 5, 44, 5, 831, DateTimeKind.Utc).AddTicks(7830), "Pending", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7828) },
                    { new Guid("575fc48e-f5df-4594-bf4c-78c526df73d0"), "Booking003", new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7836), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City W", "C789", new DateTime(1985, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "789 Oak Avenue", "District Z", "D789", "DL555555", "Alice Johnson", "1357924680", "555-123-4567", "Ward C", "W789", new DateTime(2023, 9, 27, 10, 44, 5, 831, DateTimeKind.Utc).AddTicks(7841), true, "Cash", new DateTime(2023, 9, 27, 8, 44, 5, 831, DateTimeKind.Utc).AddTicks(7840), "Confirmed", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7836) },
                    { new Guid("92274bf6-66d5-415b-a180-21225f8a60ed"), "Booking001", new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7800), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City Y", "C123", new DateTime(1980, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "123 Main Street", "District X", "D123", "DL123456", "John Doe", "1234567890", "123-456-7890", "Ward A", "W123", new DateTime(2023, 9, 27, 4, 44, 5, 831, DateTimeKind.Utc).AddTicks(7804), true, "Credit Card", new DateTime(2023, 9, 27, 2, 44, 5, 831, DateTimeKind.Utc).AddTicks(7803), "Confirmed", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7801) },
                    { new Guid("ec968489-5d89-408d-bdda-efffe84ca736"), "Booking005", new Guid("9e5333e0-1b60-4904-98f9-1e95aff995bb"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7851), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City C", "C222", new DateTime(1988, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "222 Maple Street", "District B", "D222", "DL888888", "Eve Davis", "3698521470", "888-555-1212", "Ward E", "W222", new DateTime(2023, 9, 27, 16, 44, 5, 831, DateTimeKind.Utc).AddTicks(7853), true, "PayPal", new DateTime(2023, 9, 27, 14, 44, 5, 831, DateTimeKind.Utc).AddTicks(7853), "Pending", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7851) },
                    { new Guid("f55d9385-f1bf-444f-a9ef-364d086ccf84"), "Booking004", new Guid("851fa2bf-afe1-4254-b8c8-8c9dfa41932d"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7844), "5371c83d-e0bb-48eb-9bb2-da49162d0fe9", "City B", "C101", new DateTime(1992, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "101 Pine Street", "District A", "D101", "DL777777", "Bob Smith", "2468135790", "777-987-6543", "Ward D", "W101", new DateTime(2023, 9, 27, 13, 44, 5, 831, DateTimeKind.Utc).AddTicks(7847), true, "Credit Card", new DateTime(2023, 9, 27, 11, 44, 5, 831, DateTimeKind.Utc).AddTicks(7846), "Confirmed", new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7845) }
                });

            migrationBuilder.InsertData(
                table: "CarImages",
                columns: new[] { "Id", "CarId", "CreatedAt", "Description", "IsActive", "UpdatedAt", "Url" },
                values: new object[,]
                {
                    { new Guid("309f44fa-a3f5-4fe9-8d42-c95ee741da15"), new Guid("9ec22072-ca63-4a3b-801e-d91b2617b18f"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7796), "Front view of Car2", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7797), "car2_image1.jpg" },
                    { new Guid("5bd0a58c-e9b3-4ee0-a16d-dfc0c1079597"), new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7777), "Front view of Car1", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7778), "car1_image1.jpg" },
                    { new Guid("9825bfdd-b12d-4a96-ae3b-61b243a29926"), new Guid("86049ebe-34e7-4869-be5a-0870b3f6aa62"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7790), "Interior of Car1", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7791), "car1_image2.jpg" },
                    { new Guid("afab0ac4-8563-4bf5-8bf5-ec724f7f94ad"), new Guid("6c872c52-3638-4f40-bb9d-ca60a3fbac38"), new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7787), "Back view of Car1", true, new DateTime(2023, 9, 27, 9, 44, 5, 831, DateTimeKind.Local).AddTicks(7787), "car1_image2.jpg" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_CustomerId",
                table: "Bookings",
                column: "CustomerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
