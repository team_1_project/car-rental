﻿using CarRental.Core.Models;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace CarRental.Core.Common
{
    internal class DbInitalize
    {
        public static List<Address> InitAddresses()
        {
            var workbook = new XLWorkbook("Address_value_list.xlsx");
            var worksheet = workbook.Worksheet(1);
            var i = 1;

            var addressesDataList = new List<Address>();

            foreach (var row in worksheet.RowsUsed().Skip(1)) // Skip header row
            {
                var addressData = new Address()
                {
                    Id = i,
                    WardCode = row.Cell(1).Value.ToString(),
                    Ward = row.Cell(2).Value.ToString(),
                    DistrictCode = row.Cell(3).Value.ToString(),
                    District = row.Cell(4).Value.ToString(),
                    CityCode = row.Cell(5).Value.ToString(),
                    City = row.Cell(6).Value.ToString(),
                    CreatedAt = null,
                    UpdatedAt = null,
                    IsActive = true,
                };
                i++;
                addressesDataList.Add(addressData);
            }

            return addressesDataList;
        }

        public static List<BrandModel> InitBrandModels()
        {
            var workbook = new XLWorkbook("Brand_and_model.xlsx");
            var worksheet = workbook.Worksheet(1);
            var i = 1;

            var brandModelsDataList = new List<BrandModel>();

            foreach (var row in worksheet.RowsUsed().Skip(1)) // Skip header row
            {
                var brandModelData = new BrandModel()
                {
                    Id = int.Parse(row.Cell(1).Value.ToString()),
                    Brand = row.Cell(2).Value.ToString(),
                    Model = row.Cell(3).Value.ToString(),
                    CreatedAt = null,
                    UpdatedAt = null,
                    IsActive = true,
                };
                i++;
                brandModelsDataList.Add(brandModelData);
            }

            return brandModelsDataList;
        }

        public static void SeedDataForUsers(ModelBuilder modelBuilder)
        {
            List<Address> addressData = DbInitalize.InitAddresses();
            List<BrandModel> brandModels = DbInitalize.InitBrandModels();

            List<IdentityRole> roles = new List<IdentityRole>() {
                new IdentityRole()
                {
                    Id = "BA07197E-631B-4E86-80F0-A167CB4FD6C1",
                    Name = AppRoles.Admin,
                    ConcurrencyStamp = DateTime.Now.ToString(),
                    NormalizedName = AppRoles.Admin.ToUpper()
                },
                new IdentityRole()
                {
                    Id = "234FA7FE-6C0B-4E16-9D94-3CC0EC9D8114",
                    Name = AppRoles.Customer,
                    ConcurrencyStamp = DateTime.Now.ToString(),
                    NormalizedName = AppRoles.Customer.ToUpper()
                },
                new IdentityRole()
                {
                    Id = "FB6DF12A-A1F9-43C9-B1D1-0B9085D7D285",
                    Name = AppRoles.CarOwner,
                    ConcurrencyStamp = DateTime.Now.ToString(),
                    NormalizedName = AppRoles.CarOwner.ToUpper()
                }
            };

            var passwordHasher = new PasswordHasher<ApplicationUser>();

            List<ApplicationUser> applicationUsers = new List<ApplicationUser>() {
                new ApplicationUser()
                {
                    Id = "874E5E74-B0CD-48AF-BD56-BD6366D17F2E",
                    UserName = "admin@email.com",
                    Email = $"admin@email.com",
                    EmailConfirmed = true,
                    LockoutEnabled = true,
                },
                new ApplicationUser()
                {
                    Id = "21B05714-45A3-48DB-930D-1C714FEC0E70",
                    UserName = "customer@email.com",
                    Email = $"customer@email.com",
                    EmailConfirmed = true,
                    LockoutEnabled = true,
                },
                new ApplicationUser()
                {
                    Id = "B4CA6EEF-2C72-415F-994B-A117A0E3F31C",
                    UserName = "carOwner@email.com",
                    Email = $"carOwner@email.com",
                    EmailConfirmed = true,
                    LockoutEnabled = true,
                },
            };

            foreach (var applicationUser in applicationUsers)
            {
                applicationUser.PasswordHash = passwordHasher.HashPassword(applicationUser, "123456");
                applicationUser.NormalizedEmail = applicationUser.Email.ToUpper();
                applicationUser.NormalizedUserName = applicationUser.UserName.ToUpper();
            }

            List<IdentityUserRole<string>> identityUserRoles = new List<IdentityUserRole<string>>() {
                new IdentityUserRole<string>()
                {
                    UserId = applicationUsers[0].Id.ToString(),
                    RoleId = roles[0].Id
                },
                new IdentityUserRole<string>()
                {
                    UserId = applicationUsers[1].Id.ToString(),
                    RoleId = roles[1].Id
                },
                new IdentityUserRole<string>()
                {
                    UserId = applicationUsers[2].Id.ToString(),
                    RoleId = roles[2].Id
                },
            };

            // Add these Booking instances to the modelBuilder configuration

            modelBuilder.Entity<Address>().HasData(addressData);

            modelBuilder.Entity<BrandModel>().HasData(brandModels);

            modelBuilder.Entity<IdentityRole>().HasData(roles);

            modelBuilder.Entity<ApplicationUser>().HasData(applicationUsers);

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(identityUserRoles);

        }
    }
}
