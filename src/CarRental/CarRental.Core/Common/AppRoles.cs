﻿namespace CarRental.Core.Common
{
    public class AppRoles
    {
        public const string Customer = "Customer";
        public const string CarOwner = "CarOwner";
        public const string Admin = "Admin";
    }
}
