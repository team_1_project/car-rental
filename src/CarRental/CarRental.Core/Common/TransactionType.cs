﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Core.Common
{
    public class TransactionType
    {
        public const string TopUp = "Top Up";
        public const string WithDraw = "With Draw";
        public const string Deposit = "Deposit";
        public const string Refund = "Refund";
        public const string ReceiveDeposit = "Receive Deposit";
        public const string OffsetFinalPayment = "Offset Final Payment";
    }
}
