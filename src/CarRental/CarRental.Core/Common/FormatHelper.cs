﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Core.Common
{
    public class FormatHelper
    {
        public static string FormatCurrency(decimal value)
        {
            return value.ToString("C0", new CultureInfo("vi-VN"));
        }
    }
}
