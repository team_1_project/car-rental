﻿namespace CarRental.Core.Common
{
    public class CarImagePosition
    {
        public const string Registration = "registration";
        public const string Certificate = "certificate";
        public const string Insurance = "insurance";
        public const string Front = "front";
        public const string Back = "back";
        public const string Left = "left";
        public const string Right = "right";

    }
}
